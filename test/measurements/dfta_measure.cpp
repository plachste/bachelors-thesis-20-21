#include <dfta/dfta.h>

#include <numeric>
#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <map>

using namespace std;

using namespace borovmi5;
using namespace borovmi5::algorithm;
using namespace borovmi5::dfta;
using namespace borovmi5::types;

using duration_t = unsigned long long int;

std::string treeName(types::size_t size, types::size_t rank, types::size_t depth, types::size_t id) {
	ostringstream oss;
	oss << rank << ':' << depth << '@' << size << '.' << id << ".tree";
	return oss.str();
}

Tree treeFromFile(const string& fileName) {
	ifstream treeFile(fileName, ios_base::in | ios_base::binary);
	types::size_t sz;
	treeFile.read((char*)&sz, sizeof(decltype(sz)));
	labels_t labels(sz);
	for(auto i = 0ull; i < sz; ++i)
		labels.at(i).push_back(treeFile.get());
	children_t children(sz);
	for(auto i = 0ull; i < sz; ++i) {
		auto& ch = children.at(i);
		types::size_t chsz;
		treeFile.read((char*)&chsz, sizeof(decltype(chsz)));
		ch = ParallelArray<node_t>(chsz);
		treeFile.read((char*)ch.data(), chsz * sizeof(decltype(*ch.data())));
	}
	return { labels, children };
}

struct timePair {
	duration_t real;
	duration_t cpu;
};

class TimeStat {
		vector<duration_t> history;
	public:
		void add(duration_t entry) {
			history.insert(lower_bound(history.begin(), history.end(), entry), entry);
		}

		duration_t max() const {
			return history.back();
		}

		duration_t min() const {
			return history.front();
		}

		duration_t avg() const {
			return accumulate(history.begin(), history.end(), 0) / history.size();
		}
};

using TimeStats = std::map<std::string, TimeStat>;

enum class TestType : uint8_t {
	SEQ,
	PAF,
	APAF,
	PAR,
	APAR
};

template <typename Callback>
timePair measuredCall(Callback c) {
	struct timespec rs, re, cs, ce;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &rs);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cs);
	c();
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ce);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &re);

	long long int rds = re.tv_sec - rs.tv_sec;
	long long int rdns = re.tv_nsec - rs.tv_nsec;
	duration_t rtime = rds * 1000000000ll + rdns;

	long long int cds = ce.tv_sec - cs.tv_sec;
	long long int cdns = ce.tv_nsec - cs.tv_nsec;
	duration_t ctime = cds * 1000000000ll + cdns;

	return { rtime, ctime };
}

TimeStats measureDFTA(types::size_t repetitions, locality_t k, const TA& dfta, const Tree& tree, unsigned int processorCount, TestType test) {
	TimeStats t;
	auto origP = omp_get_max_threads();
	omp_set_num_threads(processorCount);
	while(repetitions--) {
		switch(test) {
			case TestType::SEQ: {
				auto time = measuredCall(
					[k, &dfta, &tree]() {
						run(k, dfta, tree, Execution::SEQ);
					}
				);
				t["rr"].add(time.real);
				t["rc"].add(time.cpu);
				break;
			}
			case TestType::PAF:
			case TestType::PAR: {
				PreprocessedTree ptree(tree, {}, {});
				auto             ptime = measuredCall(
					[k, &tree, &ptree, &test]() {
						ptree = preprocess(k, tree, test == TestType::PAF);
					}
				);
				auto             rtime = measuredCall(
					[k, &dfta, &ptree]() {
						run(k, dfta, ptree);
					}
				);
				t["pr"].add(ptime.real);
				t["pc"].add(ptime.cpu);
				t["rr"].add(rtime.real);
				t["rc"].add(rtime.cpu);
				break;
			}
			case TestType::APAF:
			case TestType::APAR: {
				APreprocessedTree ptree(tree, {}, {});
				auto             ptime = measuredCall(
					[k, &tree, &ptree, &test]() {
						ptree = apreprocess(k, tree, test == TestType::APAF);
					}
				);
				auto             rtime = measuredCall(
					[k, &dfta, &ptree]() {
						arun(k, dfta, ptree);
					}
				);
				t["pr"].add(ptime.real);
				t["pc"].add(ptime.cpu);
				t["rr"].add(rtime.real);
				t["rc"].add(rtime.cpu);
				break;
			}
		}
	}
	omp_set_num_threads(origP);

	return t;
}

void measureTree(ostream& output, ostream& log, types::size_t repetitions, const vector<types::size_t>& ps, const vector<locality_t>& ks, types::size_t size, types::size_t rank, types::size_t depth, types::size_t id, const string& path) {
	auto tree = treeFromFile(path + "/" + treeName(size, rank, depth, id));
	ANITA dfta;
	for(auto k : ks) {
		log << "1:"; log.flush();
		log << "S"; log.flush();
		auto t = measureDFTA(repetitions, k, dfta, tree, 1, TestType::SEQ);
		output <<        'S'
		       << ',' << k
			   << ',' << size
			   << ',' << rank
			   << ',' << depth
			   << ',' << id
 			   << ',' << 0
			   << ',' << 0
			   << ',' << 0
		       << ',' << t["rr"].min()
		       << ',' << t["rr"].max()
		       << ',' << t["rr"].avg()
               << ',' << 0
               << ',' << 0
               << ',' << 0
		       << ',' << t["rc"].min()
		       << ',' << t["rc"].max()
		       << ',' << t["rc"].avg() << endl;
		log << ";"; log.flush();
		log << endl;
		for(auto p : ps) {
			log << p << ':'; log.flush();
			log << "F"; log.flush();
			t = measureDFTA(repetitions, k, dfta, tree, p, TestType::PAF);
			output <<        p << 'F'
			       << ',' << k
			       << ',' << size
			       << ',' << rank
			       << ',' << depth
			       << ',' << id
			       << ',' << t["pr"].min()
			       << ',' << t["pr"].max()
			       << ',' << t["pr"].avg()
			       << ',' << t["rr"].min()
			       << ',' << t["rr"].max()
			       << ',' << t["rr"].avg()
			       << ',' << t["pc"].min()
			       << ',' << t["pc"].max()
			       << ',' << t["pc"].avg()
			       << ',' << t["rc"].min()
			       << ',' << t["rc"].max()
			       << ',' << t["rc"].avg() << endl;
			log << ';'; log.flush();

			log << "W"; log.flush();
			t = measureDFTA(repetitions, k, dfta, tree, p, TestType::PAR);
			output <<        p << 'W'
			       << ',' << k
			       << ',' << size
			       << ',' << rank
			       << ',' << depth
			       << ',' << id
			       << ',' << t["pr"].min()
			       << ',' << t["pr"].max()
			       << ',' << t["pr"].avg()
			       << ',' << t["rr"].min()
			       << ',' << t["rr"].max()
			       << ',' << t["rr"].avg()
			       << ',' << t["pc"].min()
			       << ',' << t["pc"].max()
			       << ',' << t["pc"].avg()
			       << ',' << t["rc"].min()
			       << ',' << t["rc"].max()
			       << ',' << t["rc"].avg() << endl;
			log << ';'; log.flush();


			log << "AF"; log.flush();
			t = measureDFTA(repetitions, k, dfta, tree, p, TestType::APAF);
			output <<        p << "AF"
			       << ',' << k
			       << ',' << size
			       << ',' << rank
			       << ',' << depth
			       << ',' << id
			       << ',' << t["pr"].min()
			       << ',' << t["pr"].max()
			       << ',' << t["pr"].avg()
			       << ',' << t["rr"].min()
			       << ',' << t["rr"].max()
			       << ',' << t["rr"].avg()
			       << ',' << t["pc"].min()
			       << ',' << t["pc"].max()
			       << ',' << t["pc"].avg()
			       << ',' << t["rc"].min()
			       << ',' << t["rc"].max()
			       << ',' << t["rc"].avg() << endl;
			log << ';'; log.flush();

			log << "AW"; log.flush();
			t = measureDFTA(repetitions, k, dfta, tree, p, TestType::APAR);
			output <<        p << "AW"
			       << ',' << k
			       << ',' << size
			       << ',' << rank
			       << ',' << depth
			       << ',' << id
			       << ',' << t["pr"].min()
			       << ',' << t["pr"].max()
			       << ',' << t["pr"].avg()
			       << ',' << t["rr"].min()
			       << ',' << t["rr"].max()
			       << ',' << t["rr"].avg()
			       << ',' << t["pc"].min()
			       << ',' << t["pc"].max()
			       << ',' << t["pc"].avg()
			       << ',' << t["rc"].min()
			       << ',' << t["rc"].max()
			       << ',' << t["rc"].avg() << endl;
			log << ';'; log.flush();
			log << endl;
		}
	}
}

void usage() {
	cout << "USAGE: measure.run PATH" << endl;
}

int main(int argc, char * argv[]) {
	if(argc > 2) {
		usage();
		return 2;
	}


	/*
	 * Input format:
	 *
	 * R ... number of repetitions of single test
	 * N ... number of sets
	 * N-times {
	 *   p1 ... pi 0 ... processor counts P in set
	 *   k1 ... kj 0 ... ks K in set
	 *   s1 ... sk 0 ... sizes S in set
	 *   r1 ... rm 0 ... max ranks R in set
	 *   d1 ... dn 0 ... max depths D in set
	 *   C  ... number of trees per S x R x D
	 * }
	 *
	 * Output tree naming
	 * rank:depth@size.id.tree
	 */

	string dir(argv[1]);

	ofstream out(dir + "/measurements.csv");
	out << "p,k,size,rank,depth,id,prl,prh,prm,rrl,rrh,rrm,pcl,pch,pcm,rcl,rch,rcm" << endl;

	types::size_t R;
	cin >> R;

	unsigned long N;
	cin >> N;

	while(N--) {
		vector<types::size_t> ps;
		vector<types::size_t> ks;
		vector<types::size_t> sizes;
		vector<types::size_t> ranks;
		vector<types::size_t> depths;

		types::size_t value;
		cin >> value;
		while(value) {
			ps.push_back(value);
			cin >> value;
		}
		cin >> value;
		while(value) {
			ks.push_back(value);
			cin >> value;
		}
		cin >> value;
		while(value) {
			sizes.push_back(value);
			cin >> value;
		}
		cin >> value;
		while(value) {
			ranks.push_back(value);
			cin >> value;
		}
		cin >> value;
		while(value) {
			depths.push_back(value);
			cin >> value;
		}

		types::size_t C;
		cin >> C;

		for (auto s : sizes) {
			for (auto r : ranks) {
				for (auto d : depths) {
					for (auto i = 0ull; i < C; ++i) {
						cout << "Testing " << treeName(s, r, d, i) << "..." << endl;
						measureTree(out, cout, R, ps, ks, s, r, d, i, dir);
						cout << "Done." << endl;
					}
				}
			}
		}
		cout << "Test set done." << endl;
	}
	cout << "All tests done." << endl;
	return 0;
}


