#ifndef BOROVMI5_TEST_GENERATORS_H
#define BOROVMI5_TEST_GENERATORS_H
#include <dfta/dfta.h>
#include <types/types.h>
#include <algorithm/algorithm.h>

borovmi5::types::Tree generateTree(unsigned long depth, unsigned long size, unsigned long maxRank = std::numeric_limits<unsigned long>::max());

#endif /* BOROVMI5_TEST_GENERATORS_H */