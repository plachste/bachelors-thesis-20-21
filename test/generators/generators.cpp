#include "generators.h"
#include <random>
#include <set>
#include <stack>
#include <map>
#include <string>
#include <fstream>
#include <limits>
#include <memory>

#include <algorithm/parentheses_matching.h>
#include <math/math.h>

#include <tree/generate/RandomTreeFactory.h>
#include <arbology/exact/ExactPatternMatchingAutomaton.h>
#include <automaton/determinize/Determinize.h>
#include <automaton/simplify/Minimize.h>
#include <automaton/simplify/Normalize.h>

using namespace std;

using namespace borovmi5;
using namespace borovmi5::algorithm;
using namespace borovmi5::dfta;
using namespace borovmi5::types;

using uli = unsigned long int;

namespace {
	class Node {
			using node_ptr = std::shared_ptr<Node>;

			uli idx;
			node_ptr parent = nullptr;
			node_ptr firstChild = nullptr;
			node_ptr lastChild = nullptr;
			node_ptr rightSibling = nullptr;
			node_ptr self = nullptr;

			uli rank = 0, depth = 0;

		public:
			explicit Node(uli idx, node_ptr parent = nullptr, node_ptr rightSibling = nullptr) :
				idx(idx), parent(parent), rightSibling(rightSibling), depth(parent ? parent->depth + 1 : 0) {
			}

			static node_ptr addChild(uli idx, node_ptr parent) {
				if (parent->firstChild == nullptr) {
					parent->firstChild = parent->lastChild = std::make_shared<Node>(idx, parent);
					parent->firstChild->rightSibling = parent->firstChild;
				} else {
					parent->lastChild = parent->lastChild->rightSibling = std::make_shared<Node>(idx, parent, parent->firstChild);
				}
				++parent->rank;
				return parent->lastChild;
			}

			void rotateChildren(uli cnt) {
				if(rank) cnt %= rank;
				else cnt = 0;
				while(cnt--) {
					lastChild = firstChild;
					firstChild = firstChild->rightSibling;
				}
			}

			node_ptr getChildren() const {
				return firstChild;
			}

			node_ptr getParent() const {
				return parent;
			}

			node_ptr getNextSibling() const {
				return rightSibling;
			}

			uli getDepth() const {
				return depth;
			}

			uli getRank() const {
				return rank;
			}

			bool isRoot() const {
				return parent == nullptr;
			}

			bool isLeaf() const {
				return firstChild == nullptr;
			}

			uli& Idx() {
				return idx;
			}
	};
}

Tree generateTree(uli depth, uli size, uli maxRank) {
	if ( depth >= size ) throw std::runtime_error("Insufficient tree size.");

	if ( ( pow ( maxRank, depth + 1 ) - 1 < size ) ) throw exception::CommonException ( "Insufficient max rank." );

	mt19937 g(random_device{}());

	ParallelArray<std::shared_ptr<Node>> nodes(size);

	uli root = 0;
	nodes[root] = std::make_shared<Node>(root);
	for (int i = 1; i < depth; ++i)
		nodes[i] = Node::addChild(i, nodes[i - 1]);
	nodes[size - 1] = Node::addChild(size - 1, nodes[depth - 1]);

	int freeBeg = depth;
	int freeEnd = size - 2;

	std::uniform_int_distribution<uli> d(0, freeBeg - 1);

	while (freeEnd >= freeBeg) {
		int idx = d(g);

		if (nodes[idx]->getDepth() + 1 < depth)
			nodes[freeBeg++] = Node::addChild(freeBeg, nodes[idx]);
		else
			nodes[freeEnd--] = Node::addChild(freeEnd, nodes[idx]);

		// put parent node to end if it reached rank limit
		if (nodes[idx]->getRank() >= maxRank) {
			if(idx == root)
				root = freeEnd;
			nodes[idx]->Idx() = freeEnd;
			nodes[freeEnd--] = nodes[idx];
			nodes[--freeBeg]->Idx() = idx;
			nodes[idx] = nodes[freeBeg];
		}

		d.param(std::uniform_int_distribution<uli>::param_type(0, freeBeg - 1));
	}

	if(root) {
		auto tmp = nodes[root];
		nodes[root]->Idx() = 0;
		nodes[0]->Idx() = root;
		nodes[root] = nodes[0];
		nodes[0] = tmp;
	}

	auto c = nodes[0];
	while (!c->isLeaf()) {
		auto n = c->getChildren();
		c->rotateChildren(std::uniform_int_distribution<uli>{0, c->getRank()}(g));
		c = n;
	}

	children_t _children(size);
	labels_t _labels(size, "a");

	for(auto i = 0ull; i < size; ++i) {
		auto rank = nodes[i]->getRank();
		_children[i] = ParallelArray<node_t>(rank);
		auto c = nodes[i]->getChildren();
		for(auto j = 0ull; j < rank; ++j, c = c->getNextSibling())
			_children[i][j] = c->Idx();
	}

	return { _labels, _children };
}

void treeToFile(const Tree& t, const std::string& fileName) {
	ofstream treeFile(fileName, ios_base::out | ios_base::binary);
	auto sz = t.size();
	treeFile.write((const char*)&sz, sizeof(decltype(sz)));
	for(auto i = 0ull; i < sz; ++i)
		treeFile.put(t.label(i).front());
	for(auto i = 0ull; i < sz; ++i) {
		const auto& ch = t.children(i);
		auto chsz = ch.size();
		treeFile.write((const char*)&chsz, sizeof(decltype(chsz)));
		treeFile.write((const char*)ch.data(), chsz * sizeof(decltype(*ch.data())));
	}
}

std::string treeName(types::size_t size, types::size_t rank, types::size_t depth, types::size_t id) {
	ostringstream oss;
	oss << rank << ':' << depth << '@' << size << '.' << id << ".tree";
	return oss.str();
}

void usage() {
	cout << "USAGE: gentree.run PATH" << endl;
}

int main(int argc, char * argv[]) {
	if(argc > 2) {
		usage();
		return 2;
	}

	std::string dir(argv[1]);

	types::size_t size, rank, depth, id;

	std::cin >> size >> rank >> depth >> id;

	treeToFile(generateTree(depth, size, rank), dir + '/' + treeName(size, rank, depth, id));

	return 0;
}
