/**
 * @author Milan Borový
 * @date 23. 3. 2021
 *
 * Tree
 */

#include <types/tree.h>
#include <omp.h>

namespace borovmi5::types {
	Tree::Tree(labels_t labels, children_t children) :
	labels(std::move(labels)), childs(std::move(children)), rootNode()
	{
		ParallelArray<bool> isRoot(this->labels.size(), true);

		#pragma omp parallel for
		for(auto i = 0ull; i < this->childs.size(); ++i)
			#pragma omp parallel for
			for(auto j = 0ull; j < this->childs[i].size(); ++j)
				isRoot[this->childs[i][j]] = false;

		#pragma omp parallel for
		for(auto i = 0ull; i < this->labels.size(); ++i)
			if(isRoot[i]) rootNode = i;
	}

	const label_t& Tree::label(node_t node) const {
		return this->labels[node];
	}

	const children_t& Tree::children() const {
		return this->childs;
	}

	const ParallelArray<node_t>& Tree::children(node_t node) const {
		return this->childs[node];
	}

	types::size_t Tree::childCount(node_t node) const {
		return this->childs[node].size();
	}

	node_t Tree::root() const {
		return this->rootNode;
	}

	size_t Tree::size() const {
		return this->labels.size();
	}

	node_t Tree::operator()(node_t node, types::size_t i) const {
		return this->childs[node][i];
	}
}