/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Reverse list using OpenMP
 */

#include <algorithm/reverse_list.h>

using namespace std;
using namespace borovmi5::types;

namespace borovmi5::algorithm {
	ParallelArray<size_t> reverseList(const ParallelArray<size_t>& list) {
		ParallelArray<size_t> rev(list.size());
		#pragma omp parallel for
		for(ParallelArray<size_t>::size_type i = 0; i < list.size(); ++i)
			rev[i] = i;

		#pragma omp parallel for
		for(ParallelArray<size_t>::size_type i = 0; i < list.size(); ++i)
			if(list[i] != i)
				rev[list[i]] = i;

		return move(rev);
	}
}