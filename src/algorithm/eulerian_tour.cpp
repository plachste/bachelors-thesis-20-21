/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Eulerian tour using OpenMP
 */

#include <algorithm/eulerian_tour.h>
#include <scans/exclusive_scan.h>
#include <ranking/list_ranking.h>

using namespace std;
using namespace borovmi5;
using namespace ranking;
using namespace scans;
using namespace types;

namespace borovmi5::algorithm {
	arcs_t eulerianTour(const Tree& tree, Execution policy) {
		if(policy == Execution::SEQ)
			throw runtime_error("Not implemented.");
		auto                 nodeCount = tree.size();
		if(nodeCount < 2) return {};
		ParallelArray<unsigned long> childCount(nodeCount);
		ParallelArray<unsigned long> indices(nodeCount);

		#pragma omp parallel for
		for(unsigned long i = 0; i < nodeCount; ++i)
			childCount[i] = tree.childCount(i) * 2;

		exclusiveScan(childCount.begin(), childCount.end(), indices.begin());

		auto                 arcCount = 2 * (nodeCount - 1);
		ParallelArray<Arc>           arcs(arcCount);
		ParallelArray<unsigned long> next(arcCount);

		#pragma omp parallel for
		for(unsigned long i = 0; i < nodeCount; ++i) {
			const auto& children = tree.children(i);
			auto childs = children.size();
			#pragma omp parallel for
			for(unsigned long j = 0; j < childs; ++j) {
				auto baseIndex = indices[i] + 2 * j;
				arcs[baseIndex]     = {
					i, children[j], 0, ArcType::DOWN
				};
				arcs[baseIndex + 1] = {
					children[j], i, 0, ArcType::UP
				};

			}
		}

		#pragma omp parallel for
		for(auto i = 0ull; i < arcCount; i += 2) {
			auto from       = arcs[i].from;
			auto baseIndex  = indices[from];
			auto childIndex = (i - baseIndex) / 2;
			if(childIndex < childCount[from] / 2 - 1)
				next[i] = i + 2;
			else
				next[i] = baseIndex;
		}

		#pragma omp parallel for
		for(auto i = 1ull; i < arcCount; i += 2) {
			auto from   = arcs[i].from;
			auto children = childCount[from];
			if(children) {
				auto baseIndex = indices[from];
				next[baseIndex + children - 2] = i;
				next[i]                      = baseIndex;
			} else
				next[i] = i;
		}

		ParallelArray<unsigned long> path(arcCount);

		#pragma omp parallel for
		for(auto i = 0ull; i < arcCount; ++i)
			path[i] = next[i ^ 1ull];

		auto lastIndex = indices[tree.root()] + childCount[tree.root()] - 1;
		path[lastIndex] = lastIndex;

		auto       ranks = listRanking(path, policy);
		ParallelArray<Arc> tour(arcCount);

		#pragma omp parallel for
		for(auto i = 0ull; i < arcCount; ++i) {
			arcs[i].opposite = ranks[i ^ 1ull];
			tour[ranks[i]] = arcs[i];
		}

		return move(tour);
	}
}