#include <utility>

#include <utility>

/**
 * @author Milan Borový
 * @date 8. 3. 2021
 *
 * Main algorithm
 */

#include <algorithm/algorithm.h>
#include <dfta/dfta.h>
#include <ranking/list_ranking.h>
#include <scans/scans.h>

#include <omp.h>

#include <limits>
#include <stack>

using namespace std;
using namespace borovmi5;
using namespace algorithm;
using namespace math;
using namespace ranking;
using namespace scans;
using namespace types;

namespace borovmi5::dfta {
	void flatten(state_t in, states_t::iterator& out) {
		*(out++) = in;
	}

	arity_t DFTA::TransitionTable::getArity() const {
		return this->arity;
	}

	state_t DFTA::TransitionTable::transition(const states_t& childrenStates) const {
		auto index = 0ull;
		for(auto i = 0ull; i < this->arity; ++i)
			index += childrenStates[i] * pow(this->statec, this->arity - 1 - i);
		return this->table[index];
	}

	DFTA::DFTA(state_t maxState, States finalStates, Transitions table) :
	maxState(maxState), finalStates(std::move(finalStates)), table(std::move(table)) {}

	state_t DFTA::states() const {
		return this->maxState;
	}
	const DFTA::States& DFTA::finalSet() const {
		return this->finalStates;
	}
	state_t DFTA::transition(const types::label_t& label, const states_t& childrenStates) const {
		return this->table.at(label).transition(childrenStates);
	}
	const DFTA::TransitionTable& DFTA::transitions(const types::label_t& label) const {
		return this->table.at(label);
	}
	arity_t DFTA::arity(const types::label_t& label) const {
		return this->table.at(label).getArity();
	}
	types::labels_t DFTA::labels() const {
		labels_t labels(this->table.size());
		auto it = labels.begin();
		for(const auto& x : this->table)
			*(it++) = x.first;
		return move(labels);
	}

	state_t ANITA::transition(const types::label_t& label, const states_t& childrenStates) const {
		return std::rand();
	}

	PreprocessedTree::PreprocessedTree(types::Tree tree, DMKA_t dmka, steps_t steps) :
		tree(std::move(tree)),
		dmka(std::move(dmka)),
		steps(std::move(steps)) {
	}

	const types::Tree& PreprocessedTree::getTree() const {
		return this->tree;
	}

	const DMKA_t& PreprocessedTree::getDMKA() const {
		return this->dmka;
	}

	const steps_t& PreprocessedTree::getSteps() const {
		return this->steps;
	}

	APreprocessedTree::APreprocessedTree(types::Tree tree, DMKA_t dmka, depths_t dmk) :
		tree(std::move(tree)),
		dmka(std::move(dmka)),
		dmk(std::move(dmk)) {
	}

	const types::Tree& APreprocessedTree::getTree() const {
		return this->tree;
	}

	const DMKA_t& APreprocessedTree::getDMKA() const {
		return this->dmka;
	}

	const steps_t& APreprocessedTree::getDmk() const {
		return this->dmk;
	}

	depths_t getDepths(const arcs_t& ea) {
		auto     arcc = ea.size();
		depths_t depths(arcc);
		#pragma omp parallel for
		for(auto i = 0; i < arcc; ++i)
			depths[i] = depth_t(ea[i].type);

		inclusiveScan(depths.begin(), depths.end(), depths.begin());

		auto     nodec = arcc / 2 + 1;
		depths_t res(nodec);
		#pragma omp parallel for
		for(auto i = 0; i < arcc; ++i)
			res[ea[i].to] = depths[i];

		return move(res);
	}

	DMKA_t depthModKSort(locality_t k, const arcs_t& ea, const depths_t& depth, const types::children_t& children, bool fastRanking = false);
	steps_t computeSteps(locality_t k, const DMKA_t& DMKA, depths_t depth);
	void computeState(const TA& A, const DMKA_t& DMKA, const steps_t& step, const Tree& tree, states_t& state);
	void acomputeState(const TA& A, const DMKA_t& DMKA, const depths_t& dmk, const Tree& tree, states_t& state);

	PreprocessedTree preprocess(locality_t k, const types::Tree& tree, bool fastRanking) {
		if(!tree.size())
			return { tree, {}, {} };

		auto EA = eulerianTour(tree, fastRanking ? Execution::PAF : Execution::PAR);
		auto depth = getDepths(EA);
		auto DMKA = depthModKSort(k, EA, depth, tree.children(), fastRanking);
		auto step = computeSteps(k, DMKA, depth);

		return { tree, DMKA, step };
	}

	APreprocessedTree apreprocess(locality_t k, const types::Tree& tree, bool fastRanking) {
		if(!tree.size())
			return { tree, {}, {} };

		auto EA = eulerianTour(tree, fastRanking ? Execution::PAF : Execution::PAR);
		auto depth = getDepths(EA);
		auto DMKA = depthModKSort(k, EA, depth, tree.children(), fastRanking);
		depths_t dmk(DMKA.size());
		#pragma omp parallel for shared(dmk, depth)
		for (auto i = 0ull; i < depth.size(); ++i)
			dmk[i] = depth[DMKA[i]] % k;

		return { tree, DMKA, dmk };
	}

	states_t _runSeq(locality_t k, const TA& A, const types::Tree& tree) {
		if(!tree.size())
			return {};

		stack<pair<node_t, types::size_t>> stack;
		states_t state(tree.size());

		vector<bool> isRoot(tree.size(), true);
		for(const auto& n : tree.children())
			for(const auto& c : n)
				isRoot[c] = false;

		stack.push({ tree.root(), 0 });

		while(!stack.empty()) {
			auto top = stack.top();
			stack.pop();

			if(top.second >= tree.childCount(top.first)) {
				states_t childrenStates(tree.childCount(top.first));
				for(auto i = 0ull; i < tree.childCount(top.first); ++i)
					childrenStates[i] = state[tree(top.first, i)];
				state[top.first] = A.transition(tree.label(top.first), childrenStates);
			} else {
				stack.push({ top.first, top.second + 1 });
				stack.push({ tree(top.first, top.second), 0 });
			}
		}

		return state;
	}

	states_t _runPar(locality_t k, const TA& A, const types::Tree& tree, bool fastRanking = false) {
		if(!tree.size())
			return {};

		auto EA = eulerianTour(tree, fastRanking ? Execution::PAF : Execution::PAR);
		auto depth = getDepths(EA);
		auto DMKA = depthModKSort(k, EA, depth, tree.children(), fastRanking);
		auto step = computeSteps(k, DMKA, depth);

		states_t state(tree.size());
		computeState(A, DMKA, step, tree, state);
		computeState(A, DMKA, step, tree, state);

		return state;
	}

	states_t _arunPar(locality_t k, const TA& A, const types::Tree& tree, bool fastRanking = false) {
		if(!tree.size())
			return {};

		auto EA = eulerianTour(tree, fastRanking ? Execution::PAF : Execution::PAR);
		auto depth = getDepths(EA);
		auto DMKA = depthModKSort(k, EA, depth, tree.children(), fastRanking);
		depths_t dmk(DMKA.size());
		#pragma omp parallel for shared(dmk, depth)
		for (auto i = 0ull; i < depth.size(); ++i)
			dmk[i] = depth[DMKA[i]] % k;

		states_t state(tree.size());
		acomputeState(A, DMKA, dmk, tree, state);
		acomputeState(A, DMKA, dmk, tree, state);

		return state;
	}

	states_t run(locality_t k, const TA& A, const types::Tree& tree, algorithm::Execution policy) {
		switch(policy) {
			case Execution::SEQ: return _runSeq(k, A, tree);
			case Execution::PAF: return _runPar(k, A, tree, true);
			default: return _runPar(k, A, tree);
		}
	}

	states_t arun(locality_t k, const TA& A, const types::Tree& tree, algorithm::Execution policy) {
		switch(policy) {
			case Execution::SEQ: return _runSeq(k, A, tree);
			case Execution::PAF: return _arunPar(k, A, tree, true);
			default: return _arunPar(k, A, tree);
		}
	}

	states_t run(locality_t k, const TA& A, const PreprocessedTree& tree) {
		if(!tree.getTree().size())
			return {};

		states_t state(tree.getTree().size());
		computeState(A, tree.getDMKA(), tree.getSteps(), tree.getTree(), state);
		computeState(A, tree.getDMKA(), tree.getSteps(), tree.getTree(), state);

		return state;
	}

	states_t arun(locality_t k, const TA& A, const APreprocessedTree& tree) {
		if(!tree.getTree().size())
			return {};

		states_t state(tree.getTree().size());
		acomputeState(A, tree.getDMKA(), tree.getDmk(), tree.getTree(), state);
		acomputeState(A, tree.getDMKA(), tree.getDmk(), tree.getTree(), state);

		return state;
	}

	DMKA_t depthModKSort(locality_t k, const arcs_t& ea, const depths_t& depth, const types::children_t& children, bool fastRanking) {
		auto height = reduce(depth.begin(), depth.end(), Execution::PAR, [](depth_t a, depth_t b){ return max(a, b); });
		auto arcc = ea.size();

		ParallelArray<Par> par(arcc + 2 * height);

		#pragma omp parallel for
		for(auto i = 0ull; i < arcc; ++i)
			par[height + i] = Par(-int(ea[i].type));
		#pragma omp parallel for
		for(auto i = 0ull; i < height; ++i) {
			par[i] = Par::LEFT;
			par[height + arcc + i] = Par::RIGHT;
		}

		ParallelArray<unsigned long long int> nextPar(par.size());
		matchParentheses(par, nextPar, Execution::PAF);

		#pragma omp parallel for
		for(auto i = 0ull; i < arcc; ++i)
			if(ea[i].type == ArcType::DOWN)
				nextPar[height + i] = ea[i].opposite + height;

		#pragma omp parallel for
		for(auto i = 0ull; i < height; ++i) {
			if(i + k < height)
				nextPar[height + arcc + i] = nextPar[nextPar[height - i - k - 1]];
			else if((i + 1) % k != k - 1)
				nextPar[height + arcc + i] = nextPar[nextPar[height - ((i + 1) % k) - 1]];
		}

		auto nodeCount = arcc / 2 + 1;
		ParallelArray<node_t> order(nodeCount);
		ParallelArray<bool> isRoot(nodeCount, true);

		#pragma omp parallel for
		for(auto i = 0ull; i < arcc; ++i)
			if(ea[i].type == ArcType::UP)
				isRoot[order[ea[i].from] = ea[nextPar[nextPar[i + height]] - height].from] = false;

		node_t listRoot = 0, treeRoot = 0;
		#pragma omp parallel for
		for(auto i = 0ull; i < nodeCount; ++i) {
			if(isRoot[i]) {
				if(order[i] != i)
					listRoot = i;
				else
					treeRoot = i;
			}
		}

		order[treeRoot] = listRoot;

		DMKA_t DMKA(nodeCount);
		auto ranking = listRanking(order, fastRanking ? Execution::PAF : Execution::PAR);

		#pragma omp parallel for
		for(auto i = 0ull; i < ranking.size(); ++i)
			DMKA[ranking[i]] = i;

		return move(DMKA);
	}

	steps_t computeSteps(locality_t k, const DMKA_t& DMKA, depths_t depth) {
		auto nodeCount = DMKA.size();
		ParallelArray<bool> GE(nodeCount);
		ParallelArray<types::size_t> GEI(nodeCount);
		ParallelArray<types::size_t> group(nodeCount);

		#pragma omp parallel for
		for(auto i = 0ull; i < nodeCount; ++i)
			group[i] = depth[DMKA[i]] % k;
		#pragma omp parallel for
		for(auto i = 0ull; i < nodeCount; ++i)
			if((GE[i] = ((i == nodeCount - 1) || (group[i] != group[i + 1]))))
				GEI[i] = i;

		segmentedInclusiveScan(GEI.rbegin(), GEI.rend(), GEI.rbegin(), GE.rbegin());

		steps_t step(nodeCount);
		auto maxCPUs = omp_get_max_threads();
		#pragma omp parallel for
		for(auto i = 0ull; i < nodeCount; ++i)
			step[i] = ((GEI[i] - i) % maxCPUs) == 0 ? 1 : 0;

		inclusiveScan(step.rbegin(), step.rend(), step.rbegin());

		return move(step);
	}

	void computeState(const TA& A, const DMKA_t& DMKA, const steps_t& step, const Tree& tree, states_t& state) {
		auto maxCPUs = omp_get_max_threads();
		#pragma omp parallel
		{
			auto i = DMKA.size() - omp_get_thread_num() - 1;
			for(auto j = 1ull; j <= step[0]; ++j) {
				if(i < step.size() && step[i] == j) {
					const auto& label = tree.label(DMKA[i]);
					const auto& children = tree.children(DMKA[i]);
					auto arity = children.size();
					states_t childrenStates(arity);
					for(auto k = 0ull; k < arity; ++k)
						childrenStates[k] = state[children[k]];
					state[DMKA[i]] = A.transition(label, childrenStates);
					i -= maxCPUs;
				}
				#pragma omp barrier
			}
		}
	}

	void acomputeState(const TA& A, const DMKA_t& DMKA, const depths_t& dmk, const Tree& tree, states_t& state) {
		auto maxCPUs = omp_get_max_threads();
		#pragma omp parallel
		{
			auto i = DMKA.size() - omp_get_thread_num() - 1;
			for(auto j = dmk.back(); j <= dmk.back(); --j) {
				while(i < dmk.size() && dmk[i] == j) {
					const auto& label = tree.label(DMKA[i]);
					const auto& children = tree.children(DMKA[i]);
					auto arity = children.size();
					states_t childrenStates(arity);
					for(auto k = 0ull; k < arity; ++k)
						childrenStates[k] = state[children[k]];
					state[DMKA[i]] = A.transition(label, childrenStates);
					i -= maxCPUs;
				}
				#pragma omp barrier
			}
		}
	}
}
