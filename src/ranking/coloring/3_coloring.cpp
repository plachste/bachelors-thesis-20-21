/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * 3-coloring using OpenMP
 */

#include <ranking/coloring/3_coloring.h>
#include <ranking/coloring/6_coloring.h>

#include <algorithm/reverse_list.h>

#include <omp.h>

using namespace std;
using namespace borovmi5::algorithm;
using namespace borovmi5::types;

using ulli = unsigned long long int;

namespace borovmi5::ranking::coloring {
	ParallelArray<size_t> threeColoring(const ParallelArray<size_t>& S) {
		auto P = reverseList(S);
		auto c = sixColoring(S);
		if(c.empty()) return c;

		for(size_t k = 5; k >= 3; --k) {
			auto c_next = c;
			#pragma omp parallel for
			for(ParallelArray<size_t>::size_type i = 0; i < c.size(); ++i) {
				if(c[i] == k) {
					auto x = 7ull & ~(1ull << c[S[i]]) & ~(1ull << c[P[i]]);
					c_next[i] = x % 2 ? 0 : x % 4 ? 1 : 2;
				}
			}
			c = move(c_next);
		}
		return c;
	}
}