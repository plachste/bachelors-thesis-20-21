/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * 6-coloring using OpenMP
 */

#include <ranking/coloring/6_coloring.h>
#include <ranking/coloring/n_coloring.h>
#include <math/math.h>

#include <omp.h>

using namespace std;
using namespace borovmi5::math;
using namespace borovmi5::types;

using ulli = unsigned long long int;

namespace borovmi5::ranking::coloring {

	// extract bit b from number n
	inline const constexpr ulli bit(ulli n, ulli b) {
		return (n >> b) % 2;
	}

	// get LSB where a and b differs
	inline const constexpr ulli diffPos(ulli a, ulli b) {
		ulli diff = a ^ b;
		ulli pos  = 0ull;
		if(!diff)
			return 0ull;

		while(!(diff % 2ull)) {
			diff >>= 1ull;
			++pos;
		}

		return pos;
	}

	ParallelArray<size_t> sixColoring(const ParallelArray<size_t>& S) {
		auto c = nColoring(S.size());
		if(c.empty()) return c;

		for(ulli x = 0; x < log2Star(c.size()); ++x) {
			ParallelArray<size_t> c_next(c.size());
			#pragma omp parallel for
			for(ParallelArray<size_t>::size_type i = 0; i < c.size(); ++i) {
				ulli pi = diffPos(c[i], c[S[i]]);
				c_next[i] = 2ull * pi + bit(c[i], pi);
			}
			c = move(c_next);
		}
		return c;
	}
}