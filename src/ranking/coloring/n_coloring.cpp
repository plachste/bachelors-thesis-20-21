/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Generate n-coloring using OpenMP
 */

#include <ranking/coloring/n_coloring.h>

#include <omp.h>

using namespace std;
using namespace borovmi5::types;

namespace borovmi5::ranking::coloring {
	ParallelArray<size_t> nColoring(size_t n) {
		ParallelArray<size_t> coloring(n);
		#pragma omp parallel for
		for(ParallelArray<size_t>::size_type i = 0; i < n; ++i)
			coloring[i] = i;
		return coloring;
	}
}