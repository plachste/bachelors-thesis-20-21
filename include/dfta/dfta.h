/**
 * @author Milan Borový
 * @date 3. 3. 2021
 *
 * Main algorithm
 */

#ifndef BOROVMI5_DFTA_H
#define BOROVMI5_DFTA_H

#include <types/types.h>
#include <math/math.h>

#include <set>
#include <unordered_map>

#include <algorithm/algorithm.h>

namespace borovmi5::dfta {
	using arity_t = types::size_t;
	using depths_t = types::ParallelArray<types::depth_t>;
	using locality_t = unsigned long long int;
	using order_t = unsigned long long int;
	using DMKA_t = types::ParallelArray<order_t>;
	using state_t = unsigned long long int;
	using states_t = types::ParallelArray<state_t>;
	using step_t = unsigned long long int;
	using steps_t = types::ParallelArray<step_t>;

	void flatten(state_t in, states_t::iterator& out);
	template <typename Inner>
	void flatten(const types::ParallelArray<Inner>& in, states_t::iterator& out) {
		for(auto i = 0ull; i < std::end(in) - std::begin(in); ++i)
			flatten(in[i], out);
	}

	class TA {
		public:
			virtual state_t transition(const types::label_t& label, const states_t& childrenStates) const = 0;
	};

	class DFTA : public TA {
		public:
			class TransitionTable {
				public:
					types::size_t statec;
					arity_t arity;
					states_t table;

				public:
					template <typename Table>
					TransitionTable(types::size_t statec, arity_t arity, const Table& table):
					statec(statec), arity(arity), table(math::pow(statec, arity)) {
						auto iter = this->table.begin();
						flatten(table, iter);
					}

					arity_t getArity() const;
					state_t transition(const states_t& childrenStates) const;
			};

			using States = std::set<state_t>;
			using Transitions = std::unordered_map<types::label_t, TransitionTable>;

		private:
			state_t maxState;
			States finalStates;
			Transitions table;

		public:
			DFTA(state_t maxState, States  finalStates, Transitions  table);

			state_t states() const;
			const States& finalSet() const;
			state_t transition(const types::label_t& label, const states_t& childrenStates) const override;
			const TransitionTable& transitions(const types::label_t& label) const;
			arity_t arity(const types::label_t& label) const;
			types::labels_t labels() const;
	};

	class ANITA : public TA {
		public:
			state_t transition(const types::label_t& label, const states_t& childrenStates) const override;
	};

	class PreprocessedTree {
		protected:
			types::Tree tree;
			DMKA_t dmka;
			steps_t steps;

		public:
			PreprocessedTree() = default;
			PreprocessedTree(types::Tree tree, DMKA_t dmka, steps_t steps);

			const types::Tree& getTree() const;
			const DMKA_t& getDMKA() const;
			const steps_t& getSteps() const;
	};

	PreprocessedTree preprocess(locality_t k, const types::Tree& tree, bool fastRanking = false);
	states_t run(locality_t k, const TA& A, const types::Tree& tree, algorithm::Execution policy = algorithm::Execution::PAR);
	states_t run(locality_t k, const TA& A, const PreprocessedTree& tree);

	class APreprocessedTree {
		protected:
			types::Tree tree;
			DMKA_t dmka;
			depths_t dmk;

		public:
			APreprocessedTree() = default;
			APreprocessedTree(types::Tree tree, DMKA_t dmka, depths_t dmk);

			const types::Tree& getTree() const;
			const DMKA_t& getDMKA() const;
			const depths_t& getDmk() const;
	};

	APreprocessedTree apreprocess(locality_t k, const types::Tree& tree, bool fastRanking = false);
	states_t arun(locality_t k, const TA& A, const types::Tree& tree, algorithm::Execution policy = algorithm::Execution::PAR);
	states_t arun(locality_t k, const TA& A, const APreprocessedTree& tree);
}

#endif /* BOROVMI5_DFTA_H */
