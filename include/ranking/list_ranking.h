/**
 * @author Milan Borový
 * @date 10. 2. 2021
 *
 * List ranking
 */

#ifndef BOROVMI5_LIST_RANKING_H
#define BOROVMI5_LIST_RANKING_H

#include <types/parallel_array.h>
#include <scans/scans.h>
#include <algorithm/execution.h>
#include <algorithm/reverse_list.h>

#include "coloring/coloring.h"
#include <math/math.h>

#include <omp.h>
#include <stack>

namespace borovmi5::ranking {
	/**
	 * Sequential list ranking
	 * @tparam RAIterator input iterator type
	 * @tparam RankingIterator ranking iterator type
	 * @param first first element of range containing successor list
	 * @param last first element after last element of range containing successor list
	 * @param ranking first element of range for ranking
	 */
	template <typename RAIterator, typename RankingIterator>
	void _listRankingSeq(RAIterator first, RAIterator last, RankingIterator ranking) {
		using value_type = typename RankingIterator::value_type;
		auto size = last - first;
		if(size < 1)
			return;

		for(auto   it = first; it != last; ++it)
			ranking[*it] = 1;
		value_type cur   = 0, sum = 0;

		for(value_type i = 0; i < size; ++i) {
			if(!ranking[i]) {
				cur = i;
				break;
			}
		}

		while(cur != first[cur])
			ranking[cur = first[cur]] = ++sum;
	}

	/**
	 * Parallel list ranking using pointer-jumping
	 * @tparam RAIterator input iterator type
	 * @tparam RankingIterator ranking iterator type
	 * @param first first element of range containing successor list
	 * @param last first element after last element of range containing successor list
	 * @param ranking first element of range for ranking
	 */
	template <typename RAIterator, typename RankingIterator>
	void _listRankingPJ(RAIterator first, RAIterator last, RankingIterator ranking) {
		using namespace types;
		using value_type = typename RAIterator::value_type;
		using rank_type = typename RankingIterator::value_type;
		auto size = last - first; // get input arraySize

		if(size < 1)
			return;

		ParallelArray<value_type> sC(first, last), sN(first, last); // copy successor list
		ParallelArray<rank_type>  rTmp(size, 1); // prepare ranking in memory
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			ranking[i] = 1;

		value_type  root = value_type();
		ParallelArray<bool> isRoot(size, true);
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			isRoot[first[i]] = false;
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			if(isRoot[i])
				root = i;

		ranking[root] = rTmp[root] = 0;

		for(auto dist = 1ull; dist < size; dist <<= 1ull) {
			#pragma omp parallel for shared(dist, ranking, rTmp, sC, sN)
			for(auto i = 0ull; i < size; ++i) {
				auto succ = sC[i];

				if(i == succ)
					continue;

				rTmp[succ] = ranking[succ] + ranking[i];
				sN[i]      = sC[succ];
				if(sN[i] == sC[i])
					sN[i] = i;
			}

			sC = sN;
			#pragma omp parallel for
			for(auto i = 0; i < size; ++i)
				ranking[i] = rTmp[i];
		}
	}

	/**
	 * Parallel list ranking using work-efficient algorithm
	 * @tparam RAIterator input iterator type
	 * @tparam RankingIterator ranking iterator type
	 * @param first first element of range containing successor list
	 * @param last first element after last element of range containing successor list
	 * @param ranking first element of range for ranking
	 */
	template <typename RAIterator, typename RankingIterator>
	void _listRankingWE(RAIterator first, RAIterator last, RankingIterator ranking)	{
		unsigned long long int size = last - first;
		unsigned long long int limitSize = std::max(size / math::log2(size), 5ull);

		if(size < 1)
			return;

		using namespace scans;
		using namespace types;

		struct _memory {
			unsigned long long int index;
			typename RAIterator::value_type successor;
			typename RAIterator::value_type predecessor;
			typename RankingIterator::value_type rank;
		};

		std::stack<ParallelArray<_memory>> tmp;

		ParallelArray<unsigned long int> successor(first, last);
		auto predecessor = algorithm::reverseList(successor);
		ParallelArray<bool> indicator(size);
		ParallelArray<unsigned long long int> n(size);

		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			ranking[i] = 1;

		typename RankingIterator::value_type  root = typename RankingIterator::value_type();
		ParallelArray<bool> isRoot(size, true);
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			isRoot[first[i]] = false;
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			if(isRoot[i])
				root = i;

		ranking[root] = 0;

		while(size > limitSize) {
			auto c = coloring::threeColoring(successor);
			#pragma omp parallel for
			for(auto i = 0ull; i < size; ++i)
				n[i] = (indicator[i] = (c[i] < c[predecessor[i]] && c[i] < c[successor[i]]));
			inclusiveScan(n.begin(), n.begin() + size, n.begin());
			ParallelArray<_memory> lvlTmp(n[size - 1]);
			#pragma omp parallel for
			for(auto i = 0ull; i < size; ++i)
			{
				if(indicator[i]) {
					lvlTmp[n[i] - 1] = { i, successor[i], predecessor[i], ranking[i] };
					ranking[successor[i]] += ranking[i];
					if(predecessor[i] != i)
						successor[predecessor[i]] = successor[i];
					if(successor[i] != i)
						predecessor[successor[i]] = predecessor[i];
				}
			}
			#pragma omp parallel for
			for(auto i = 0ull; i < size; ++i)
				n[i] = !indicator[i];
			exclusiveScan(n.begin(), n.begin() + size, n.begin());
			auto successorCpy = successor;
			auto predecessorCpy = predecessor;
			ParallelArray<typename RankingIterator::value_type> rankingCpy(ranking, ranking + size);
			#pragma omp parallel for
			for(auto i = 0ull; i < size; ++i) {
				if(!indicator[i]) {
					successor[n[i]]   = n[successorCpy[i]];
					predecessor[n[i]] = n[predecessorCpy[i]];
					ranking[n[i]]     = rankingCpy[i];
				}
			}
			tmp.emplace(std::move(lvlTmp));
			size -= tmp.top().size();
		}
		ParallelArray<typename RankingIterator::value_type> ranks(size);
		ParallelArray<typename RankingIterator::value_type> orderedRanks(size);
		_listRankingPJ(successor.begin(), successor.begin() + size, ranks.begin());
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			orderedRanks[ranks[i]] = ranking[i];
		inclusiveScan(orderedRanks.begin(), orderedRanks.end(), orderedRanks.begin());
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			ranking[i] = orderedRanks[ranks[i]];
		while(!tmp.empty()) {
			auto& lvlTmp = tmp.top();
			size += lvlTmp.size();
			#pragma omp parallel for
			for(auto i = 0ull; i < size; ++i) {
				n[i] = 1;
				indicator[i] = false;
			}
			#pragma omp parallel for
			for(auto i = 0ull; i < lvlTmp.size(); ++i) {
				n[lvlTmp[i].index] = 0;
				indicator[lvlTmp[i].index] = true;
			}
			exclusiveScan(n.begin(), n.begin() + size, n.begin());
			decltype(n) reverseN(n.size());
			#pragma omp parallel for
			for(auto i = 0ull; i < size; ++i)
				if(!indicator[i])
					reverseN[n[i]] = i;
			auto successorCpy = successor;
			auto predecessorCpy = predecessor;
			ParallelArray<typename RankingIterator::value_type> rankingCpy(ranking, ranking + size);
			#pragma omp parallel for
			for(auto i = 0ull; i < size - lvlTmp.size(); ++i) {
				successor[reverseN[i]] = reverseN[successorCpy[i]];
				predecessor[reverseN[i]] = reverseN[predecessorCpy[i]];
				ranking[reverseN[i]] = rankingCpy[i];
			}
			#pragma omp parallel for
			for(auto i = 0ull; i < lvlTmp.size(); ++i) {
				successor[lvlTmp[i].predecessor] = lvlTmp[i].index;
				predecessor[lvlTmp[i].successor] = lvlTmp[i].index;
				successor[lvlTmp[i].index] = lvlTmp[i].successor;
				predecessor[lvlTmp[i].index] = lvlTmp[i].predecessor;
				ranking[lvlTmp[i].index] = ranking[lvlTmp[i].predecessor] + lvlTmp[i].rank;
			}
			tmp.pop();
		}
	}

	/**
	 * List ranking
	 * @tparam RAIterator input iterator type
	 * @tparam RankingIterator ranking iterator type
	 * @param first first element of range containing successor list
	 * @param last first element after last element of range containing successor list
	 * @param ranking first element of range for ranking
	 * @param policy execution policy
	 */
	template <typename RAIterator, typename RankingIterator>
	void listRanking(RAIterator first, RAIterator last, RankingIterator ranking, algorithm::Execution policy) {
		using namespace algorithm;
		switch(policy) {
			case Execution::SEQ: _listRankingSeq(first, last, ranking);
				break;
			case Execution::PAF: _listRankingPJ(first, last, ranking);
				break;
			default: _listRankingWE(first, last, ranking);
		}
	}

	/**
	 * List ranking
	 * @tparam Container successor list type
	 * @tparam Ranking ranking container type
	 * @param successorList successor list
	 * @param ranking container for ranking
	 * @param policy execution policy
	 */
	template <typename Container, typename Ranking>
	void listRanking(
		const Container& successorList,
		Ranking& ranking,
		algorithm::Execution policy = algorithm::Execution::PAR
	) {
		listRanking(successorList.begin(), successorList.end(), ranking.begin(), policy);
	}

	/**
	 * List ranking
	 * @tparam Container successor list type
	 * @tparam Ranking ranking container type
	 * @param successorList successor list
	 * @param policy execution policy
	 * @return resulting ranking
	 */
	template <typename Container, typename Ranking = types::ParallelArray <size_t>>
	Ranking listRanking(const Container& successorList, algorithm::Execution policy = algorithm::Execution::PAR) {
		Ranking r(successorList.end() - successorList.begin());
		listRanking(successorList, r, policy);
		return r;
	}
}

#endif /* BOROVMI5_LIST_RANKING_H */