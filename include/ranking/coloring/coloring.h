/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Coloring using OpenMP
 */

#ifndef BOROVMI5_COLORING_H
#define BOROVMI5_COLORING_H

#include "n_coloring.h"
#include "6_coloring.h"
#include "3_coloring.h"

#endif /* BOROVMI5_N_COLORING_H */