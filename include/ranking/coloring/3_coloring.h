/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * 3-coloring using OpenMP
 */

#ifndef BOROVMI5_3_COLORING_H
#define BOROVMI5_3_COLORING_H

#include <types/parallel_array.h>

namespace borovmi5::ranking::coloring {
	/**
	 * Generate 3-coloring of list given as array of successors
	 * @param S successor array
	 * @return array representing 3-coloring
	 */
	borovmi5::types::ParallelArray<std::size_t> threeColoring(const borovmi5::types::ParallelArray<std::size_t>& S);
}

#endif /* BOROVMI5_3_COLORING_H */