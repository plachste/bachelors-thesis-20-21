/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Generate n-coloring using OpenMP
 */

#ifndef BOROVMI5_N_COLORING_H
#define BOROVMI5_N_COLORING_H

#include <types/parallel_array.h>

namespace borovmi5::ranking::coloring {
	/**
	 * Generates n-coloring of n elements
	 * @param n number of elements
	 * @return array of size n representing n-coloring (a[i] = i)
	 */
	borovmi5::types::ParallelArray<std::size_t> nColoring(std::size_t n);
}

#endif /* BOROVMI5_N_COLORING_H */