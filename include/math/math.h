/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Math functions
 */

#ifndef BOROVMI5_MATH_H
#define BOROVMI5_MATH_H

#include <exception>

namespace borovmi5::math {
	class math_error : public std::runtime_error {
		public:
			explicit math_error(const std::string& msg): runtime_error(msg) {}
			explicit math_error(const char * msg): runtime_error(msg) {}
	};

	/**
	 * log2*(n) := min{ i | log^(i)(n) <= 1 }
	 * @param n n
	 * @return log2*(n)
	 */
	constexpr unsigned long long int log2Star(unsigned long long int n) {
		if(n < 1)
			throw math_error("Value not in log2* domain");
		if(n == 1)
			return 0;
		if(n <= 2)
			return 1;
		if(n <= 4)
			return 2;
		if(n <= 16)
			return 3;
		if(n <= 65536)
			return 4;
		return 5;
	}

	/**
	 * Logarithm log_2 x
	 * @param x x
	 * @return log_2 x
	 */
	constexpr unsigned long long int log2(unsigned long long int x) {
		if(x < 1)
			throw math_error("Value not in log2 domain");
		unsigned long long int res = 0;
		while((x >> res) != 1ull)
			++res;
		return res;
	}

	/**
	 * Power x^y
	 * @param x x
	 * @param y y
	 * @return x^y
	 */
	constexpr unsigned long long int pow(unsigned long long int x, unsigned long long int y) {
		unsigned long long int res = 1;

		while(y) {
			if(y & 1ull) res *= x;
			y >>= 1ull;
			x *= x;
		}

		return res;
	}
}

#endif /* BOROVMI5_MATH_H */