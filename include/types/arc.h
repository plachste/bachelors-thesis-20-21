/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Arc
 */

#ifndef BOROVMI5_ARC_H
#define BOROVMI5_ARC_H

namespace borovmi5::types {
	enum ArcType {
		UP = -1, DOWN = 1
	};

	using node_t = unsigned long long int;

	struct Arc {
		node_t  from;
		node_t  to;
		node_t  opposite;
		ArcType type;

		bool operator==(const Arc& that) const {
			return this->from     == that.from     &&
				   this->to       == that.to       &&
				   this->opposite == that.opposite &&
				   this->type     == that.type;
		}
	};
}

#endif /* BOROVMI5_ARC_H */
