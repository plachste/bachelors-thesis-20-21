/**
 * @author Milan Borový
 * @date 4. 1. 2021
 *
 * Parallel array using OpenMP
 */

#ifndef BOROVMI5_PARALLEL_ARRAY_H
#define BOROVMI5_PARALLEL_ARRAY_H

#include <algorithm>
#include <limits>
#include <stdexcept>
#include <memory>
#include <omp.h>

namespace borovmi5::types {

	/**
	 * @brief Simple array wrapper.
	 * @tparam T value type
	 */
	template <class T, class Allocator = std::allocator<T>> class ParallelArray {
			using self = ParallelArray;
		public:
			using allocator_type = Allocator;
			using value_type = typename allocator_type::value_type;
			using size_type = typename allocator_type::size_type;
			using difference_type = typename allocator_type::difference_type;
			using reference = value_type&;
			using const_reference = const value_type&;
			using pointer = value_type*;
			using const_pointer = const value_type*;

			class iterator : public std::iterator<
				std::random_access_iterator_tag,
				value_type,
				long,
				pointer,
				reference
			> {
					using self = iterator;
					pointer element;
				public:
					explicit iterator(pointer element = nullptr) : element(element) {
					}

					bool operator==(const self& that) const {
						return this->element == that.element;
					}

					bool operator!=(const self& that) const {
						return this->element != that.element;
					}

					reference operator*() {
						return *this->element;
					}

					const_reference operator*() const {
						return *this->element;
					}

					pointer operator->() {
						return element;
					}

					const_pointer operator->() const {
						return element;
					}

					self& operator++() {
						++element;
						return *this;
					}

					self operator++(int) {
						auto it = self(element);
						++element;
						return it;
					}

					self& operator--() {
						--element;
						return *this;
					}

					self operator--(int) {
						auto it = self(element);
						--element;
						return it;
					}

					self operator+(long n) const {
						return self(this->element + n);
					}

					friend self operator+(long n, const self& it) {
						return it + n;
					}

					self operator-(long n) const {
						return self(this->element - n);
					}

					long operator-(const self& that) const {
						return this->element - that.element;
					}

					bool operator<(const self& that) const {
						return this->element < that.element;
					}

					bool operator>(const self& that) const {
						return this->element > that.element;
					}

					bool operator<=(const self& that) const {
						return this->element <= that.element;
					}

					bool operator>=(const self& that) const {
						return this->element >= that.element;
					}

					self& operator+=(long n) {
						this->element += n;
						return *this;
					}

					self& operator-=(long n) {
						this->element -= n;
						return *this;
					}

					reference operator[](long n) {
						return *(this->element + n);
					}
			};

			class const_iterator : public std::iterator<
				std::random_access_iterator_tag,
				value_type,
				long,
				const_pointer,
				const_reference
			> {
					using self = const_iterator;
					const_pointer element;
				public:
					explicit const_iterator(const_pointer element = nullptr) : element(element) {
					}

					bool operator==(const self& that) const {
						return this->element == that.element;
					}

					bool operator!=(const self& that) const {
						return this->element != that.element;
					}

					const_reference operator*() const {
						return *this->element;
					}

					const_pointer operator->() const {
						return element;
					}

					self& operator++() {
						++element;
						return *this;
					}

					self operator++(int) {
						auto it = self(element);
						++element;
						return it;
					}

					self& operator--() {
						++element;
						return *this;
					}

					self operator--(int) {
						auto it = self(element);
						++element;
						return it;
					}

					self operator+(long n) const {
						return self(this->element + n);
					}

					friend self operator+(long n, const self& it) {
						return it + n;
					}

					self operator-(long n) const {
						return self(this->element - n);
					}

					long operator-(const self& that) const {
						return this->element - that.element;
					}

					bool operator<(const self& that) const {
						return this->element < that.element;
					}

					bool operator>(const self& that) const {
						return this->element > that.element;
					}

					bool operator<=(const self& that) const {
						return this->element <= that.element;
					}

					bool operator>=(const self& that) const {
						return this->element >= that.element;
					}

					self& operator+=(long n) {
						this->element += n;
						return *this;
					}

					self& operator-=(long n) {
						this->element -= n;
						return *this;
					}

					const_reference operator[](long n) {
						return *(this->element + n);
					}
			};

			using reverse_iterator = std::reverse_iterator<iterator>;
			using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		protected:
			allocator_type allocator;
			size_type arraySize;
			pointer array;

		public:
			/**
			 * Default constructor.
			 * Constructs an empty container.
			 */
			ParallelArray() noexcept(noexcept(allocator_type())) :
			allocator(allocator_type()),
			arraySize(),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
			}

			/**
			 * Constructor.
			 * Constructs an empty array with the given allocator.
			 * @param allocator given allocator
			 */
			explicit ParallelArray(const allocator_type& allocator):
			allocator(allocator),
			arraySize(),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
			}

			/**
			 * Constructor.
			 * Constructs the container with given count copies of elements of given value.
			 * @param count given count
			 * @param value given value
			 * @param allocator allocator to initialize with
			 */
			ParallelArray(size_type count, const_reference value, const allocator_type& allocator = allocator_type()) :
			allocator(allocator),
			arraySize(count),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, array + i, value);
			}

			/**
			 * Constructor.
			 * Constructs the container with given count of default-inserted instances of T.
			 * @param count given count
			 * @param allocator allocator to initialize with
			 */
			explicit ParallelArray(size_type count, const allocator_type& allocator = allocator_type()) :
				allocator(allocator),
				arraySize(count),
				array()
			{
				array = this->allocator.allocate(this->arraySize);
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, array + i);
			}

			/**
			 * Constructor.
			 * Constructs the container with the contents of the range [first, last)
			 * @tparam InputIt input iterator type
			 * @param first lower bound
			 * @param last upper bound
			 * @param allocator given allocator
			 */
			template <class InputIt>
			ParallelArray(InputIt first, InputIt last, const allocator_type& allocator = allocator_type()) :
			allocator(allocator),
			arraySize(std::distance(first, last)),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
				InputIt * lst = nullptr;
				#pragma omp parallel
				{
					auto p = omp_get_num_threads();
					auto t = omp_get_thread_num();
					#pragma omp single
					{
						lst = new InputIt[p + 1];
						lst[p] = last;
					}
					auto it = first;
					auto index = arraySize * t / p;
					std::advance(it, index);
					#pragma omp barrier
					lst[t] = it;
					#pragma omp barrier
					while(it != lst[t + 1]) {
						std::allocator_traits<allocator_type>::construct(this->allocator, this->array + index, *it);
						++index; ++it;
					}
				}
			}

			/**
			 * Copy constructor.
			 * @param that instance of Array to copy
			 */
			ParallelArray(const self& that) :
			allocator(that.allocator),
			arraySize(that.arraySize),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, this->array + i, that.array[i]);
			}

			/**
			 * Constructor.
			 * Constructs the container as copy of that with given allocator
			 * @param that given container
			 * @param allocator given allocator
			 */
			ParallelArray(const self& that, const allocator_type& allocator) :
			allocator(allocator),
			arraySize(that.arraySize),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, this->array + i, that.array[i]);
			}

			/**
			 * Move constructor.
			 * @param that instance of Array to move
			 */
			ParallelArray(self&& that) noexcept :
			allocator(that.allocator),
			arraySize(that.arraySize),
			array(that.array)
			{
				that.array = nullptr;
				that.arraySize = 0;
			}

			/**
			 * Constructor.
			 * Move constructs the container with given allocator.
			 * @param that given container
			 * @param allocator given allocator
			 */
			ParallelArray(self&& that, const allocator_type& allocator) :
			allocator(allocator),
			arraySize(that.arraySize),
			array(that.array)
			{
				that.array = nullptr;
				that.arraySize = 0;
			}

			/**
			 * Constructor.
			 * Construct new Array with given values.
			 * @param init values
			 */
			ParallelArray(std::initializer_list<value_type> init, const allocator_type& allocator = allocator_type()) :
			allocator(allocator),
			arraySize(init.size()),
			array()
			{
				array = this->allocator.allocate(this->arraySize);
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, this->array + i, *(init.begin() + i));
			}

			/**
			 * Destructor.
			 */
			~ParallelArray() {
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
			}

			/**
			 * Copy assignment operator.
			 * @param that instance of Array to copy
			 * @return *this
			 */
			self& operator=(const self& that) {
				if(&that == this)
					return *this;
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
				this->array = this->allocator.allocate(that.arraySize);
				this->arraySize  = that.arraySize;
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, this->array + i, that.array[i]);
				return *this;
			};

			/**
			 * Move assignment operator.
			 * @param that instance of Array to move
			 * @return *this
			 */
			self& operator=(self&& that) noexcept {
				if(&that == this)
					return *this;
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
				this->array = that.array;
				this->arraySize  = that.arraySize;
				that.array  = nullptr;
				that.arraySize = 0;
				return *this;
			}

			/**
			 * Assignment operator.
			 * Assign values from initializer list to the container.
			 * @param init given initializer list
			 * @return *this
			 */
			self& operator=(const std::initializer_list<value_type>& init) {
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
				this->array = allocator.allocate(init.size());
				this->arraySize  = init.size();
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, this->array + i, *(init.begin() + i));
				return *this;
			}

			/**
			 * Replaces the contents with given count copies of given value
			 * @param count given count
			 * @param value given value
			 */
			void assign(size_type count, const_reference value) {
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
				this->array = allocator.allocate(count);
				this->arraySize = count;
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					construct(this->allocator, this->array + i, value);
			}

			/**
			 * Replaces the contents with given range [first, last)
			 * @param first lower bound
			 * @param last upper bound
			 */
			template <typename InputIt>
			void assign(InputIt first, InputIt last) {
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
				this->arraySize = std::distance(first, last);
				this->array = this->allocator.allocate(this->arraySize);

				InputIt * lst = nullptr;
				#pragma omp parallel
				{
					auto p = omp_get_num_threads();
					auto t = omp_get_thread_num();
					#pragma omp single
					{
						lst = new InputIt[p + 1];
						lst[p] = last;
					}
					auto it = first;
					auto index = arraySize * t / p;
					std::advance(it, index);
					#pragma omp barrier
					lst[t] = it;
					#pragma omp barrier
					while(it != lst[t + 1]) {
						std::allocator_traits<allocator_type>::construct(this->array + index, *it);
						++index; ++it;
					}
				}
			}

			/**
			 * Replaces the contents with contents of given initializer list
			 * @param init initializer list
			 */
			void assign(const std::initializer_list<value_type>& init) {
				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::destroy(this->allocator, this->array + i);
				this->allocator.deallocate(this->array, this->arraySize);
				this->array = this->allocator.allocate(init.size());
				this->arraySize  = init.size();
				#pragma omp parallel for
				for(int i = 0; i < this->arraySize; ++i)
					std::allocator_traits<allocator_type>::construct(this->allocator, this->array + i, *(init.begin() + i));
			}

			/**
			 * Get allocator
			 * @return allocator
			 */
			allocator_type get_allocator() const noexcept {
				return this->allocator;
			}

			/**
			 * Access element at given index.
			 * Does boundary checking.
			 */
			reference at(size_type pos) {
				if(pos >= this->arraySize)
					throw std::out_of_range("Index out of range.");
				return this->array[pos];
			}

			/**
			 * Access element at given index.
			 * Does boundary checking.
			 */
			const_reference at(size_type pos) const {
				if(pos >= this->arraySize)
					throw std::out_of_range("Index out of range.");
				return this->array[pos];
			}

			/**
			 * Access element at given index.
			 * Does boundary checking.
			 */
			reference operator[](size_type pos) {
				if(pos >= this->arraySize)
					throw std::out_of_range("Index out of range.");
				return this->array[pos];
			}

			/**
			 * Access element at given index.
			 * Does boundary checking.
			 */
			const_reference operator[](size_type pos) const {
				if(pos >= this->arraySize)
					throw std::out_of_range("Index out of range.");

				return this->array[pos];
			}

			/**
			 * Access the first element.
			 */
			reference front() {
				return this->array[0];
			}

			/**
			 * Access the first element.
			 */
			const_reference front() const {
				return this->array[0];
			}

			/**
			 * Access the last element.
			 */
			reference back() {
				return this->array[this->arraySize - 1];
			}

			/**
			 * Access the last element
			 */
			const_reference back() const {
				return this->array[this->arraySize - 1];
			}

			/**
			 * Get an iterator to the beginning.
			 * @return iterator to the first element
			 */
			iterator begin() noexcept {
				return iterator(this->array);
			}

			/**
			 * Get an iterator to the beginning.
			 * @return iterator to the first element
			 */
			const_iterator begin() const noexcept {
				return const_iterator(this->array);
			}

			/**
			 * Get a const iterator to the beginning.
			 * @return iterator to the first element
			 */
			const_iterator cbegin() const noexcept {
				return const_iterator(this->array);
			}

			/**
			 * Get an iterator to the end.
			 * @return iterator to the element following the last element
			 */
			iterator end() noexcept {
				return iterator(this->array + this->arraySize);
			}

			/**
			 * Get an iterator to the end.
			 * @return iterator to the element following the last element
			 */
			const_iterator end() const noexcept {
				return const_iterator(this->array + this->arraySize);
			}

			/**
			 * Get a const_iterator to the end.
			 * @return iterator to the element following the last element
			 */
			const_iterator cend() const noexcept {
				return const_iterator(this->array + this->arraySize);
			}

			/**
			 * Get a reverse iterator to the beginning.
			 * @return reverse iterator to the first element
			 */
			reverse_iterator rbegin() noexcept {
				return reverse_iterator(iterator(this->array + this->arraySize));
			}

			/**
			 * Get a reverse iterator to the beginning.
			 * @return reverse iterator to the first element
			 */
			const_reverse_iterator rbegin() const noexcept {
				return const_reverse_iterator(const_iterator(this->array + this->arraySize));
			}

			/**
			 * Get a const reverse iterator to the beginning.
			 * @return reverse iterator to the first element
			 */
			const_reverse_iterator crbegin() const noexcept {
				return const_reverse_iterator(const_iterator(this->array + this->arraySize));
			}

			/**
			 * Get a reverse iterator to the end.
			 * @return reverse iterator to the element following the last element
			 */
			reverse_iterator rend() noexcept {
				return reverse_iterator(iterator(this->array));
			}

			/**
			 * Get a reverse iterator to the end.
			 * @return reverse iterator to the element following the last element
			 */
			const_reverse_iterator rend() const noexcept {
				return const_reverse_iterator(const_iterator(this->array));
			}

			/**
			 * Get a const reverse iterator to the end.
			 * @return reverse iterator to the element following the last element
			 */
			const_reverse_iterator crend() const noexcept {
				return const_reverse_iterator(const_iterator(this->array));
			}

			/**
			 * Empty test
			 * @return true if array is empty
			 */
			bool empty() const {
				return this->arraySize == 0;
			}

			/**
			 * Get array size.
			 * @return array size
			 */
			size_type size() const {
				return this->arraySize;
			}

			/**
			 * Maximal possible size.
			 * @return max size
			 */
			size_type max_size() const {
				return this->allocator.max_size();
			}

			/**
			 * Swap contents of two arrays
			 * @param that other array
			 */
			void swap(self& that) {
				this->array ^= that.array ^= this->array ^= that.array;
				this->arraySize ^= that.arraySize ^= this->arraySize ^= that.arraySize;
			}

			/**
			 * Swap contents of two arrays
			 * @param a first array
			 * @param b second array
			 */
			friend void swap(self& a, self& b) {
				a.array ^= b.array ^= a.array ^= b.array;
				a.arraySize ^= b.arraySize ^= a.arraySize ^= b.arraySize;
			}

			/**
			 * Equality test
			 * @param that other array
			 * @return true if that has same number of elements
			 * and for each pair a, b with same index of elements
			 * expression a == b is true
			 */
			bool operator==(const self& that) const {
				if(this->arraySize != that.arraySize) return false;
				bool isSame = true;

				#pragma omp parallel for
				for(auto i = 0ull; i < this->arraySize; ++i) {
					if(!isSame)	continue;
					if(this->array[i] != that.array[i])
						isSame = false;
				}

				return isSame;
			}

			/**
			 * Non-equality test
			 * @param that other array
			 * @return !(a == b)
			 */
			bool operator!=(const self& that) const {
				return !(*this == that);
			}

			pointer data() {
				return this->array;
			}

			const_pointer data() const {
				return this->array;
			}
	};
}

#endif /* BOROVMI5_PARALLEL_ARRAY_H */