/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Structures
 */

#ifndef BOROVMI5_STRUCTURES_H
#define BOROVMI5_STRUCTURES_H

#include "arc.h"
#include "common.h"
#include "parallel_array.h"
#include "tree.h"

#endif /* BOROVMI5_STRUCTURES_H */
