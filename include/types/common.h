/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Basic types
 */

namespace borovmi5::types {
	using depth_t = unsigned long long int;
	using node_t = unsigned long long int;
	using size_t = unsigned long long int;
}