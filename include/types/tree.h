/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Tree
 */

#ifndef BOROVMI5_TREE_H
#define BOROVMI5_TREE_H

#include "common.h"
#include "parallel_array.h"

namespace borovmi5::types {
	using children_t = types::ParallelArray<types::ParallelArray<types::node_t>>;
	using label_t = std::string;
	using labels_t = types::ParallelArray<label_t>;

	class Tree {
			labels_t   labels;
			children_t childs;
			node_t     rootNode;

		public:
			Tree(labels_t labels, children_t children);

			const label_t& label(node_t node) const;
			const children_t& children() const;
			const ParallelArray<node_t>& children(node_t node) const;
			size_t childCount(node_t node) const;
			node_t root() const;
			size_t size() const;
			node_t operator()(node_t node, size_t i) const;
	};
}

#endif /* BOROVMI5_TREE_H */