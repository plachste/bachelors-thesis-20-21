/**
 * @author Milan Borový
 * @date 25. 3. 2021
 *
 * Plus operator influenced by segment flag
 */

#ifndef BOROVMI5_SEGMENTED_PLUS_H
#define BOROVMI5_SEGMENTED_PLUS_H

namespace borovmi5::scans {
	/**
	 *
	 * @tparam T
	 * @tparam S
	 */
	template <typename T, typename S>
	struct SegmentedPlus {
		using result_type = T;
		using first_argument_type = T;
		using second_argument_type = T;
		using third_argument_type = S;
		using fourth_argument_type = T;

		constexpr result_type operator()(const first_argument_type& a, const second_argument_type& b, const third_argument_type& s, const fourth_argument_type& i) {
			if(s) return i + b;
			return a + b;
		}
	};
}

#endif /* BOROVMI5_SEGMENTED_PLUS_H */
