/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Scans and reduction
 */

#ifndef BOROVMI5_SCANS_H
#define BOROVMI5_SCANS_H

#include "exclusive_scan.h"
#include "inclusive_scan.h"
#include "reduce.h"

#endif /* BOROVMI5_SCANS_H */
