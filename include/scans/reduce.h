/**
 * @author Milan Borový
 * @date 14. 1. 2021
 *
 * Reduction
 */

#ifndef BOROVMI5_REDUCE_H
#define BOROVMI5_REDUCE_H

#include <algorithm/execution.h>
#include <types/parallel_array.h>

#include <omp.h>

#include <algorithm>

namespace borovmi5::scans {
	/**
	 * Sequential reduction
	 * @tparam InputIt iterator type
	 * @tparam ReduceOp reduction operator type
	 * @param first first element of range to reduce
	 * @param last first element after last element of range to reduce
	 * @param op reduction operator
	 * @param init initial value
	 * @return result of reduction of range [first,last)
	 */
	template <typename InputIt, typename ReduceOp = std::plus<typename InputIt::value_type>>
	constexpr typename InputIt::value_type _reduceSeq(
		InputIt first,
		InputIt last,
		ReduceOp op = ReduceOp(),
		typename InputIt::value_type init = typename InputIt::value_type()) {
		if(first == last) return init;
		while(first != last)
			init = op(init, *(first++));
		return init;
	}

	/**
	 * Parallel reduction using OpenMP
	 * @tparam InputIt iterator type
	 * @param first first element of range to reduce
	 * @param last first element after last element of range to reduce
	 * @param op reduction operator
	 * @param init initial value
	 * @return result of reduction of range [first,last)
	 */
	template <typename InputIt, typename ReduceOp = std::plus<typename InputIt::value_type>>
	typename InputIt::value_type _reducePar(
		InputIt first,
		InputIt last,
		ReduceOp op = ReduceOp(),
		typename InputIt::value_type init = typename InputIt::value_type()) {
		size_t max_p = omp_get_max_threads(); // get max number of threads
		size_t n     = last - first; // arraySize of input
		if(!n) return init;
		borovmi5::types::ParallelArray<typename InputIt::value_type> tmp(max_p); // initialize helper array

		// populate helper array
		size_t p = std::min(max_p, n);
		#pragma omp parallel shared(n, p, tmp, op, first)
		{
			auto t = omp_get_thread_num(); // id of thread
			if(t < n) {

				// reduce to helper array sequentially
				tmp[t] = _reduceSeq(first + n * t / p, first + n * (t + 1) / p, op);
			}
		}

		size_t dist = 1; // distance of 2 reduced elements in current step

		for(n = tmp.size() / 2; n; n >>= 1ull) { // use half of threads in each step
			#pragma omp parallel shared(tmp, dist)
			{
				auto t = omp_get_thread_num(); // id of thread
				if(t < n) {
					tmp[t * dist * 2] = op(tmp[t * dist * 2], tmp[t * dist * 2 + dist]);
				}
			}
			dist <<= 1ull;
		}

		return op(init, tmp[0]);
	}

	/**
	 * Reduction
	 * @tparam InputIt iterator type
	 * @tparam ReduceOp reduction operator type
	 * @param first first element of range to reduce
	 * @param last first element after last element of range to reduce
	 * @param policy execution policy
	 * @param op reduction operator
	 * @param init initial value
	 * @return result of reduction of container c with reduction operator op
	 */
	template <typename InputIt, typename ReduceOp = std::plus<typename InputIt::value_type>>
	typename InputIt::value_type reduce(
		InputIt first,
		InputIt last,
		borovmi5::algorithm::Execution policy = borovmi5::algorithm::Execution::PAR,
		ReduceOp op = ReduceOp(),
		typename InputIt::value_type init = typename InputIt::value_type()
	) {
		using namespace borovmi5::algorithm;
		if(policy == Execution::SEQ)
			return _reduceSeq(first, last, op, init);
		else
			return _reducePar(first, last, op, init);
	}
}

#endif /* BOROVMI5_REDUCE_H */