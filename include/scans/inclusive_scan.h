/**
 * @author Milan Borový
 * @date 27. 1. 2021
 *
 * (Segmented) Inclusive scan
 */

#ifndef BOROVMI5_INCLUSIVE_SCAN_H
#define BOROVMI5_INCLUSIVE_SCAN_H

#include "segmented_plus.h"

#include <algorithm/execution.h>
#include <types/parallel_array.h>

#include <omp.h>

#include <algorithm>
#include <stack>

namespace borovmi5::scans {
	/**
	 * Sequential inclusive scan
	 * @tparam InputIt input iterator type
	 * @tparam OutputIt output iterator type
	 * @tparam BinaryOp binary operator type
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results in. Must have at least last - first elements.
	 * @param op binary operator to use for scan
	 * @param init initial value
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename BinaryOp = std::plus<typename InputIt::value_type>>
	void _inclusiveScanSeq(
		InputIt first,
		InputIt last,
		OutputIt out,
		BinaryOp op = BinaryOp(),
		typename InputIt::value_type init = typename InputIt::value_type()) {
		while(first != last) {
			init = *out = op(init, *(first++));
			++out;
		}
	}

	/**
	 * Parallel inclusive scan using Hillis-Steele algorithm
	 * @tparam InputIt input iterator type
	 * @tparam OutputIt output iterator type
	 * @tparam BinaryOp binary operator type
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results in. Must have at least last - first elements.
	 * @param op binary operator to use for scan
	 * @param init initial value.
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename BinaryOp = std::plus<typename InputIt::value_type>>
	void _inclusiveScanPJ(
		InputIt first,
		InputIt last,
		OutputIt out,
		BinaryOp op = BinaryOp(),
		typename InputIt::value_type init = typename InputIt::value_type()) {
		size_t                                                       inputSize = last - first; // arraySize of input
		size_t                                                       maxCPUs   = std::min(
			size_t(omp_get_max_threads()),
			inputSize
		); // maximum # of CPU to use
		if(!inputSize) return;
		borovmi5::types::ParallelArray<typename InputIt::value_type> mem(maxCPUs); // temporary memory

		/*
		 * Divide input to maxCPUs parts and compute prefix sum sequentially for each separately.
		 * Store sum of each of parts to mem for later use in Hillis-Steele algorithm.
		 */
		#pragma omp parallel shared(inputSize, maxCPUs, out, op, first, mem)
		{
			auto tid = omp_get_thread_num(); // thread id

			_inclusiveScanSeq(
				first + inputSize * tid / maxCPUs,
				first + inputSize * (tid + 1) / maxCPUs,
				out + inputSize * tid / maxCPUs,
				op,
				tid ? typename InputIt::value_type() : init
			);
			mem[tid] = out[inputSize * (tid + 1) / maxCPUs - 1];
		}

		size_t dist = 1; // distance between processed values

		/*
		 * Apply pointer-jumping
		 */
		for(auto count = maxCPUs - dist; count; count = maxCPUs < dist ? 0 : maxCPUs - dist) { // use half of threads in each step
			#pragma omp parallel shared(mem, dist)
			{
				auto tid  = omp_get_thread_num(); // thread id
				if(tid < count) {
					auto left = mem[tid], right = mem[tid + dist]; // store current value
					#pragma omp barrier
					mem[tid + dist] = op(left, right); // compute next value
				} else {
					#pragma omp barrier
				}
			}
			dist <<= 1ull; // increase distance
		}

		/*
		 * Add partial sums from Hillis-Steele algorithm back to original array
		 */
		#pragma omp parallel shared(inputSize, maxCPUs, out, op, mem)
		{
			auto     tid = omp_get_thread_num(); // thread id
			if(tid < maxCPUs - 1) {
				auto     hi  = out + inputSize * (tid + 2) / maxCPUs; // upper bound
				auto     val = mem[tid]; // partial sum to add
				for(auto lo  = out + inputSize * (tid + 1) / maxCPUs; lo != hi; ++lo)
					*lo = op(val, *lo);
			}
		}
	}

	/**
	 * Parallel inclusive scan using work-efficient algorithm
	 * @tparam InputIt input iterator type
	 * @tparam OutputIt output iterator type
	 * @tparam BinaryOp binary operator type
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results in. Must have at least last - first elements.
	 * @param op binary operator to use for scan
	 * @param init initial value.
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename BinaryOp = std::plus<typename InputIt::value_type>>
	void _inclusiveScanWE(
		InputIt first,
		InputIt last,
		OutputIt out,
		BinaryOp op = BinaryOp(),
		typename InputIt::value_type init = typename InputIt::value_type()) {
		size_t                                                       inputSize = last - first; // arraySize of input
		size_t                                                       maxCPUs   = std::min(
			size_t(omp_get_max_threads()),
			inputSize
		); // maximum # of CPU to use
		if(!inputSize) return;
		borovmi5::types::ParallelArray<typename InputIt::value_type> mem(maxCPUs); // temporary memory

		auto orig = omp_get_max_threads();
		omp_set_num_threads(maxCPUs);

		/*
		 * Divide input to maxCPUs parts and compute prefix sum sequentially for each separately.
		 * Store sum of each of parts to mem for later use in work-efficient algorithm.
		 */
		#pragma omp parallel shared(inputSize, maxCPUs, out, op, first, mem)
		{
			auto tid = omp_get_thread_num(); // thread id

			_inclusiveScanSeq(
				first + inputSize * tid / maxCPUs,
				first + inputSize * (tid + 1) / maxCPUs,
				out + inputSize * tid / maxCPUs,
				op,
				tid ? typename InputIt::value_type() : init
			);
			mem[tid] = out[inputSize * (tid + 1) / maxCPUs - 1];
		}

		size_t             dist = 1; // distance between processed values
		std::stack<size_t> sizes;

		/*
		 * Up-sweep step of work-efficient algorithm
		 */
		for(auto count = maxCPUs; count / 2; count = (count + 1) / 2) {
			sizes.push(count); // store # of values to process for later use in down-sweep
			#pragma omp parallel shared(mem, dist)
			{
				auto tid = omp_get_thread_num(); // thread id
				if(tid < count / 2) {
					auto idx = (tid + 1) * dist * 2 - 1; // index of right child
					auto cor = std::min(idx, maxCPUs - 1); // corrected index of right child
					mem[cor] = op(mem[idx - dist], mem[cor]); // compute next value
				}
			}
			dist <<= 1ull; // increase distance
		}

		dist >>= 1ull; // decrease distance

		/*
		 * Clear step of work-efficient algorithm
		 */
		mem[maxCPUs - 1] = 0;

		/*
		 * Down-sweep step of work-efficient algorithm
		 */
		while(!sizes.empty()) {
			// proceed sizes from up-sweep step in reverse order
			auto count = sizes.top();
			sizes.pop();

			#pragma omp parallel shared(mem, dist)
			{
				auto tid = omp_get_thread_num(); // thread id
				if(tid < count / 2) {
					auto idx = (tid + 1) * dist * 2 - 1; // index of right child
					auto cor = std::min(idx, maxCPUs - 1); // corrected index of right child
					auto tmp = mem[idx - dist]; // temporarily store left child
					#pragma omp barrier
					mem[idx - dist] = mem[cor]; // swap left and right child
					mem[cor] += tmp; // add left child to right child
				} else {
					#pragma omp barrier
				}
			}
			dist >>= 1ull; // decrease distance
		}

		/*
		 * Add partial sums from work-efficient algorithm back to original array
		 */
		#pragma omp parallel shared(inputSize, maxCPUs, out, op, mem)
		{
			if(maxCPUs > 1) {
				auto     tid = omp_get_thread_num(); // thread id
				if(tid < maxCPUs - 1) {
					auto     hi  = out + inputSize * (tid + 2) / maxCPUs; // upper bound
					auto     val = mem[tid + 1]; // partial sum to add
					for(auto lo  = out + inputSize * (tid + 1) / maxCPUs; lo != hi; ++lo)
						*lo = op(val, *lo);
				}
			}
		}

		omp_set_num_threads(orig);
	}

	/**
	 * Inclusive scan
	 * @tparam InputRAIterator input iterator type
	 * @tparam OutputRAIterator output iterator type
	 * @tparam BinaryOp binary operator type
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results in. Must have at least last - first elements.
	 * @param policy execution policy
	 * @param op binary operator to use for scan
	 * @param init initial value.
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename BinaryOp = std::plus<typename InputIt::value_type>>
	void inclusiveScan(
		InputIt first,
		InputIt last,
		OutputIt out,
		borovmi5::algorithm::Execution policy = borovmi5::algorithm::Execution::PAR,
		BinaryOp op = BinaryOp(),
		typename InputIt::value_type init = typename InputIt::value_type()
	) {
		using namespace borovmi5::algorithm;
		switch(policy) {
			case Execution::SEQ: _inclusiveScanSeq(first, last, out, op, init);
				break;
			case Execution::PAF: _inclusiveScanPJ(first, last, out, op, init);
				break;
			default: _inclusiveScanWE(first, last, out, op, init);
				break;
		}
	}

	/**
	 * Sequential segmented inclusive scan
	 * @tparam InputIt input iterator type
	 * @tparam OutputIt output iterator type
	 * @tparam Op operator accepting previous value, current value, segment indicator and initial value
	 * @tparam SegmentIt segment iterator type
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results. Must have at least last - first elements.
	 * @param segment first element of range of segments. True indicates start of new segment.
	 * @param op operator to use for scan
	 * @param init initial value
	 * @return true if contains more than 1 segment
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename SegmentIt,
		typename Op = SegmentedPlus<typename InputIt::value_type, typename SegmentIt::value_type>>
	bool _segmentedInclusiveScanSeq(
		InputIt first,
		InputIt last,
		OutputIt out,
		SegmentIt segment,
		Op op = Op(),
		bool segmentBreaks = true,
		typename InputIt::value_type init = typename InputIt::value_type()) {
		auto sum      = init;
		bool multiple = false;
		while(first != last) {
			multiple |= *segment;
			sum = *out = op(sum, *(first++), *(segment++), init);
			++out;
		}
		return multiple;
	}

	/**
	 * Parallel segmented inclusive scan using Hillis-Steele algorithm
	 * @tparam InputIt input iterator type
	 * @tparam OutputIt output iterator type
	 * @tparam Op operator accepting previous value, current value, segment indicator and initial value
	 * @tparam SegmentIt segment iterator type
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results in. Must have at least last - first elements.
	 * @param segment first element of range of segments. True indicates start of new segment.
	 * @param op binary operator to use for scan
	 * @param init initial value.
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename SegmentIt,
		typename Op = SegmentedPlus<typename InputIt::value_type, typename SegmentIt::value_type>>
	void _segmentedInclusiveScanPJ(
		InputIt first,
		InputIt last,
		OutputIt out,
		SegmentIt segment,
		Op op = Op(),
		bool segmentBreaks = true,
		typename InputIt::value_type init = typename InputIt::value_type()) {
		using namespace borovmi5::types;
		size_t                                      inputSize = last - first; // arraySize of input
		size_t                                      maxCPUs   = std::min(
			size_t(omp_get_max_threads()),
			inputSize
		); // maximum # of CPU to use
		if(!inputSize) return;
		ParallelArray<typename InputIt::value_type> mem(maxCPUs); // temporary memory
		ParallelArray<bool>                         segMem(maxCPUs); // temporary memory for segments

		/*
		 * Divide input to maxCPUs parts and compute prefix sum sequentially for each separately.
		 * Store sum of each of parts to mem for later use in Hillis-Steele algorithm.
		 */
		#pragma omp parallel shared(inputSize, maxCPUs, out, segment, op, first, mem, segMem)
		{
			auto tid = omp_get_thread_num(); // thread id

			segMem[tid] = _segmentedInclusiveScanSeq(
				first + inputSize * tid / maxCPUs,
				first + inputSize * (tid + 1) / maxCPUs,
				out + inputSize * tid / maxCPUs,
				segment + inputSize * tid / maxCPUs,
				op,
				segmentBreaks,
				tid ? typename InputIt::value_type() : init
			);
			mem[tid]    = out[inputSize * (tid + 1) / maxCPUs - 1];
		}

		size_t dist = 1; // distance between processed values

		/*
		 * Apply pointer-jumping
		 */
		for(auto count = maxCPUs - dist; count; count = maxCPUs < dist ? 0 : maxCPUs - dist) { // use half of threads in each step
			#pragma omp parallel shared(mem, dist)
			{
				auto tid     = omp_get_thread_num(); // thread id
				if(tid < count) {
					auto left    = mem[tid], right = mem[tid + dist]; // store current value
					auto seg     = segMem[tid + dist];
					auto prevSeg = segMem[tid];
					#pragma omp barrier
					mem[tid + dist] = op(left, right, seg && segmentBreaks, init); // compute next value
					segMem[tid + dist] |= prevSeg;
				} else {
					#pragma omp barrier
				}
			}
			dist <<= 1ull; // increase distance
		}

		/*
		 * Add partial sums from Hillis-Steele algorithm back to original array
		 */
		#pragma omp parallel shared(inputSize, maxCPUs, out, op, mem)
		{
			auto     tid = omp_get_thread_num(); // thread id
			if(tid < maxCPUs - 1) {
				auto     hi  = out + inputSize * (tid + 2) / maxCPUs; // upper bound
				auto     val = mem[tid]; // partial sum to add
				auto     seg = segment + inputSize * (tid + 1) / maxCPUs;
				for(auto lo  = out + inputSize * (tid + 1) / maxCPUs; lo != hi && (!*(seg++) || !segmentBreaks); ++lo)
					*lo = op(val, *lo, typename SegmentIt::value_type(), init);
			}
		}
	}

	/**
	 * Segmented inclusive scan
	 * @tparam InputIt input iterator type
	 * @tparam OutputIt output iterator type
	 * @tparam SegmentIt segment iterator type
	 * @tparam Op operator accepting previous value, current value, segment indicator and initial value
	 * @param first first element of range to scan
	 * @param last first element after last element of range to scan
	 * @param out first element of range to store results in. Must have at least last - first elements.
	 * @param segment first element of range of segments. True indicates start of new segment.
	 * @param policy execution policy
	 * @param op binary operator to use for scan
	 * @param init initial value.
	 */
	template <
		typename InputIt,
		typename OutputIt,
		typename SegmentIt,
		typename Op = SegmentedPlus<typename InputIt::value_type, typename SegmentIt::value_type>>
	void segmentedInclusiveScan(
		InputIt first,
		InputIt last,
		OutputIt out,
		SegmentIt segment,
		borovmi5::algorithm::Execution policy = borovmi5::algorithm::Execution::PAR,
		Op op = Op(),
		bool segmentBreaks = true,
		typename InputIt::value_type init = typename InputIt::value_type()
	) {
		using namespace borovmi5::algorithm;
		if(policy == Execution::SEQ)
			_segmentedInclusiveScanSeq(first, last, out, segment, op, segmentBreaks, init);
		else
			_segmentedInclusiveScanPJ(first, last, out, segment, op, segmentBreaks, init);
	}
}

#endif /* BOROVMI5_INCLUSIVE_SCAN_H */