/**
 * @author Milan Borový
 * @date 15. 2. 2021
 *
 * Parentheses matching
 */

#ifndef BOROVMI5_PARENTHESES_MATCHING_H
#define BOROVMI5_PARENTHESES_MATCHING_H

#include <algorithm/merge_sort.h>
#include <scans/scans.h>

#include <functional>
#include <cmath>
#include <stack>

namespace borovmi5::algorithm {
	/**
	 * Parenthesis type
	 */
	enum class Par {
			LEFT = 1, RIGHT = -1
	};

	/**
	 * Sequential parentheses matching
	 * @tparam RAIterator input iterator type
	 * @tparam MatchIterator match iterator type
	 * @param first first element of range containing parentheses
	 * @param last first element after last element of range containing parentheses
	 * @param match first element of range for matching parenthesis index
	 * @return true if are all parentheses matched
	 */
	template <typename RAIterator, typename MatchIterator>
	bool _parenthesesMatchingSeq(RAIterator first, RAIterator last, MatchIterator match) {
		std::stack<size_t> lpars; // left parentheses
		auto               size    = last - first;
		bool               isValid = true;
		if(!size) return true;

		for(auto i = 0; i < size; ++i) {
			match[i] = i;

			if(first[i] == Par::LEFT)
				lpars.push(i);
			else if(first[i] == Par::RIGHT) {
				if(lpars.empty())
					isValid = false;
				else {
					match[match[i] = lpars.top()] = i;
					lpars.pop();
				}
			} else
				isValid = false;
		}

		return isValid && lpars.empty();
	}

	/**
	 * Parentheses matching submatch subroutine using stable sort to match parentheses
	 * @tparam RAIterator input iterator type
	 * @tparam MatchIterator match iterator type
	 * @param first first element of range containing parentheses
	 * @param last first element after last element of range containing parentheses
	 * @param depth first element of range containing parentheses depths
	 * @param match first element of range for matching parenthesis index
	 */
	template <typename RAIterator, typename DepthIterator, typename MatchIterator>
	void _parenthesesMatchingSubmatch(RAIterator first, RAIterator last, DepthIterator depth, MatchIterator match) {
		using namespace types;
		using depth_type = typename DepthIterator::value_type;
		using ParDepth = struct _ {
			Par        par;
			depth_type depth;
			size_t     index;
		};

		auto size = last - first;
		if(!size) return;

		ParallelArray <ParDepth> tmp(size);

		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			tmp[i] = {first[i], depth[i], i};

		mergeSort(
			tmp, Execution::PAR, [](const ParDepth& a, const ParDepth& b) {
				return a.depth < b.depth || (a.depth == b.depth && a.index < b.index);
			}
		);

		#pragma omp parallel for
		for(auto i = 0ull; i < size - 1; ++i) {
			if(tmp[i].par != Par::LEFT)
				continue;
			if(tmp[i].depth != tmp[i + 1].depth)
				continue;
			if(tmp[i + 1].par != Par::RIGHT)
				continue;

			match[match[tmp[i].index] = tmp[i + 1].index] = tmp[i].index;
		}
	}

	/**
	 * Parallel parentheses matching
	 * @tparam RAIterator input iterator type
	 * @tparam MatchIterator match iterator type
	 * @param first first element of range containing parentheses
	 * @param last first element after last element of range containing parentheses
	 * @param match first element of range for matching parenthesis index
	 * @return true if are all parentheses matched
	 */
	template <typename RAIterator, typename MatchIterator>
	bool _parenthesesMatchingPar(RAIterator first, RAIterator last, MatchIterator match) {
		using namespace scans;
		using namespace types;
		auto size    = last - first;
		bool isValid = true;

		if(!size) return true;

		ParallelArray <size_t> depth(size);
		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i) {
			depth[i] = size_t(first[i]);
			match[i] = i;
		}

		inclusiveScan(depth.begin(), depth.end(), depth.begin());

		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			if(first[i] == Par::RIGHT)
				++depth[i];

		_parenthesesMatchingSubmatch(first, last, depth, match);

		#pragma omp parallel for
		for(auto i = 0ull; i < size; ++i)
			if(match[i] == i)
				isValid = false;

		return isValid;
	}

	/**
	 * Work-optimal parallel parentheses matching
	 * @tparam RAIterator input iterator type
	 * @tparam MatchIterator match iterator type
	 * @param first first element of range containing parentheses
	 * @param last first element after last element of range containing parentheses
	 * @param match first element of range for matching parenthesis index
	 * @return true if are all parentheses matched
	 */
	template <typename RAIterator, typename MatchIterator>
	bool _parenthesesMatchingWE(RAIterator first, RAIterator last, MatchIterator match) {
		using namespace types;
		using namespace scans;
		using P = typename RAIterator::value_type;
		using M = typename MatchIterator::value_type;
		auto size = last - first;
		unsigned long long int maxCPUs = omp_get_max_threads();

		if(!size) return true;

		ParallelArray<size_t> indices(size);
		ParallelArray<size_t> left(maxCPUs);
		ParallelArray<size_t> right(maxCPUs);

		#pragma omp parallel shared(size, maxCPUs, match, indices)
		{
			auto tid = omp_get_thread_num(); // thread id
			auto cpus = omp_get_num_threads();

			std::stack<size_t> unmatched;

			auto lo = size * tid / cpus;
			auto cur = lo;
			auto hi = size * (tid + 1) / cpus;

			while(cur < hi) {
				if(first[cur] == Par::LEFT)
					unmatched.push(cur);
				else {
					if(!unmatched.empty()){
						match[cur] = unmatched.top();
						match[unmatched.top()] = cur;
						unmatched.pop();
					}
					else
						indices[lo + right[tid]++] = cur;
				}
				++cur;
			}

			while(!unmatched.empty()) {
				indices[hi - 1 - left[tid]++] = unmatched.top();
				unmatched.pop();
			}
		}

		auto dist = 1ull;

		for(auto count = maxCPUs; count / 2; count = (count + 1) / 2) {
			#pragma omp parallel
			{
				auto tid = omp_get_thread_num();
				if(tid < count / 2) {
					auto l     = tid * dist * 2;
					auto lsize = dist;
					auto r     = l + dist;
					auto rsize = std::min(dist, maxCPUs - r);

					auto toMatch = std::min(left[l], right[r]);
					auto lbase   = size * (l + lsize) / maxCPUs - 1;
					auto rbase   = size * r / maxCPUs;
					#pragma omp parallel for
					for(auto i = 0ull; i < toMatch; ++i) {
						auto lind = indices[lbase - i];
						auto rind = indices[rbase + i];

						match[lind] = rind;
						match[rind] = lind;
					}

					if(left[l] > toMatch) {
						rbase = size * (r + rsize) / maxCPUs - 1 - left[r];
						#pragma omp parallel for
						for(auto i = toMatch; i < left[l]; ++i)
							indices[rbase - i + toMatch] = indices[lbase - i];
						left[l] += left[r] - toMatch;
					} else if(right[r] > toMatch) {
						lbase = size * l / maxCPUs + right[l];
						#pragma omp parallel for
						for(auto i = toMatch; i < right[r]; ++i)
							indices[lbase + i - toMatch] = indices[rbase + i];
						right[l] += right[r] - toMatch;
						left[l]                          = 0;
					} else {
						left[l] = left[r];
					}
				}
			}
			dist <<= 1ull; // increase distance
		}

		return !left[0] && !right[0];
	}


	/**
	 * Parentheses matching
	 * @tparam RAIterator input iterator type
	 * @tparam MatchIterator match iterator type
	 * @param first first element of range containing parentheses
	 * @param last first element after last element of range containing parentheses
	 * @param match first element of range for matching parenthesis index
	 * @param policy execution policy
	 * @return true if are all parentheses matched
	 */
	template <typename RAIterator, typename MatchIterator>
	bool matchParentheses(RAIterator first, RAIterator last, MatchIterator match, Execution policy) {
		using namespace borovmi5::algorithm;
		switch(policy) {
			case Execution::SEQ: return _parenthesesMatchingSeq(first, last, match);
			case Execution::PAF: return _parenthesesMatchingPar(first, last, match);
			default: return _parenthesesMatchingWE(first, last, match);
		}
	}

	/**
	 * Parentheses matching
	 * @tparam ParContainer parentheses container type
	 * @tparam MatchContainer match container type
	 * @param par container of parentheses to match
	 * @param match container for indices of matching parentheses
	 * @param policy execution policy
	 * @return true if all parentheses matched
	 */
	template <typename ParContainer, typename MatchContainer>
	bool matchParentheses(const ParContainer& par, MatchContainer& match, Execution policy = Execution::PAR) {
		return matchParentheses(par.begin(), par.end(), match.begin(), policy);
	}
}

#endif /* BOROVMI5_PARENTHESES_MATCHING */
