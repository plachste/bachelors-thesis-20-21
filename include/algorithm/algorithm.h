/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Algorithm functions
 */

#ifndef BOROVMI5_ALGORITHM_H
#define BOROVMI5_ALGORITHM_H

#include "eulerian_tour.h"
#include "execution.h"
#include "merge_sort.h"
#include "parentheses_matching.h"
#include "reverse_list.h"

#endif /* BOROVMI5_ALGORITHM_H */