/**
 * @author Milan Borový
 * @date 2. 3. 2021
 *
 * Eulerian tour using OpenMP
 */

#ifndef BOROVMI5_EULERIAN_TOUR_H
#define BOROVMI5_EULERIAN_TOUR_H

#include <algorithm/execution.h>

#include <types/parallel_array.h>
#include <types/arc.h>
#include <types/tree.h>

namespace borovmi5::algorithm {
	using arcs_t = types::ParallelArray<types::Arc>;

	/**
	 * Construct eulerian tour from tree given as children array.
	 * @param children tree given as children array
	 * @return linked list containing eulerian tour
	 */
	arcs_t eulerianTour(const types::Tree& tree, Execution policy = Execution::PAR);
}

#endif /* BOROVMI5_EULERIAN_TOUR_H */