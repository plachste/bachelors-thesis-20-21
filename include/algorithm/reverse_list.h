/**
 * @author Milan Borový
 * @date 11. 1. 2021
 *
 * Reverse list using OpenMP
 */

#ifndef BOROVMI5_REVERSE_LIST_H
#define BOROVMI5_REVERSE_LIST_H

#include <types/parallel_array.h>

namespace borovmi5::algorithm {
	/**
	 * Reverse list.
	 * Create array of predecessors from array of successors and vice versa.
	 * @param S successor array
	 * @return predecessor array
	 */
	borovmi5::types::ParallelArray<std::size_t> reverseList(const borovmi5::types::ParallelArray<std::size_t>& list);
}

#endif /* BOROVMI5_REVERSE_LIST_H */