/**
 * @author Milan Borový
 * @date 27. 1. 2021
 *
 * Execution policy for algorithms
 */

#ifndef BOROVMI5_EXECUTION_H
#define BOROVMI5_EXECUTION_H

namespace borovmi5::algorithm {
	enum class Execution {
			SEQ, //!< Sequential execution
			PAR, //!< Parallel execution
			PAF, //!< Fast parallel execution
			PWE //!< Work-efficient parallel execution
	};
}

#endif /* BOROVMI5_EXECUTION_H */
