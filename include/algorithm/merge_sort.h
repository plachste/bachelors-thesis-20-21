/**
 * @author Milan Borový
 * @date 15. 2. 2021
 *
 * Merge sort
 */

#ifndef BOROVMI5_MERGE_SORT_H
#define BOROVMI5_MERGE_SORT_H

#include <functional>
#include <stack>

#include "execution.h"

#include <omp.h>

namespace borovmi5::algorithm {

	/**
	 * Merge 2 sorted lists sequentially
	 * @tparam InputForwardIterator input iterator type
	 * @tparam OutputForwardIterator output iterator type
	 * @tparam Comparator comparator type
	 * @param left first element of first sorted list
	 * @param leftEnd first element after last element of first sorted list
	 * @param right first element of second sorted list
	 * @param rightEnd first element after last element of second sorted list
	 * @param out first element of output range to store merged list in
	 * @param cmp compare operator used for sorting
	 */
	template <
		typename InputForwardIterator,
		typename OutputForwardIterator,
		typename Comparator = std::less<typename InputForwardIterator::value_type>>
	void _mergeSeq(
		InputForwardIterator left,
		InputForwardIterator leftEnd,
		InputForwardIterator right,
		InputForwardIterator rightEnd,
		OutputForwardIterator out,
		Comparator cmp = Comparator()) {
		using namespace types;
		using value_type = typename InputForwardIterator::value_type;

		ParallelArray <value_type> l(left, leftEnd), r(right, rightEnd);
		auto               li = 0ull, ri = 0ull;

		while(li < l.size() && ri < r.size())
			if(cmp(l[li], r[ri]))
				*(out++) = l[li++];
			else
				*(out++) = r[ri++];
		while(li < l.size())
			*(out++)     = l[li++];
		while(ri < r.size())
			*(out++)     = r[ri++];
	}

	/**
	 * Sequential merge sort
	 * @tparam InputRAIterator input iterator type
	 * @tparam OutputRAIterator output iterator type
	 * @tparam Comparator comparator type
	 * @param first first element of range to sort
	 * @param last first element after last element of range to sort
	 * @param out first element of output range
	 * @param cmp compare operator used for sorting
	 */
	template <
		typename InputRAIterator,
		typename OutputRAIterator,
		typename Comparator = std::less<typename InputRAIterator::value_type>>
	void _mergeSortSeq(
		InputRAIterator first,
		InputRAIterator last,
		OutputRAIterator out,
		Comparator cmp = Comparator()) {
		using value_type = typename InputRAIterator::value_type;

		auto size   = last - first;
		auto dist   = size / 2;
		auto mid    = first + dist; // select middle index
		auto outMid = out + dist;
		auto outEnd = out + size;

		if(first < last - 1) {
			_mergeSortSeq(first, mid, out, cmp);
			_mergeSortSeq(mid, last, outMid, cmp);
			_mergeSeq(out, outMid, outMid, outEnd, out, cmp);
		} else if(first < last) {
			*out = *first;
		}
	}

	/**
	 * Trivial parallelisation of merge sort
	 * @tparam InputRAIterator input iterator type
	 * @tparam OutputRAIterator output iterator type
	 * @tparam Comparator comparator type
	 * @param first first element of range to sort
	 * @param last first element after last element of range to sort
	 * @param out first element of output range
	 * @param cmp compare operator used for sorting
	 */
	template <
		typename InputRAIterator,
		typename OutputRAIterator,
		typename Comparator = std::less<typename InputRAIterator::value_type>>
	void _mergeSortPar(
		InputRAIterator first,
		InputRAIterator last,
		OutputRAIterator out,
		Comparator cmp = Comparator()) {
		using value_type = typename InputRAIterator::value_type;

		auto size   = last - first;
		auto dist   = size / 2;
		auto mid    = first + dist; // select middle index
		auto outMid = out + dist;
		auto outEnd = out + size;

		if(first < last - 1) {
			// split list to 2 similarly-sized lists and sort them
			#pragma omp parallel sections
			{
				#pragma omp section
				_mergeSortPar(first, mid, out, cmp);
				#pragma omp section
				_mergeSortPar(mid, last, outMid, cmp);
			}
			// merge them back
			_mergeSeq(out, outMid, outMid, outEnd, out, cmp);
		} else if(first < last) {
			*out = *first;
		}
	}

	/**
	 * Merge sort
	 * @tparam InputRAIterator input iterator type
	 * @tparam OutputRAIterator output iterator type
	 * @tparam Comparator compare operator type
	 * @param first first element of range to sort
	 * @param last first element after last element of range to sort
	 * @param out first element of output range
	 * @param policy execution policy
	 * @param cmp compare operator used for sorting
	 */
	template <
		typename InputRAIterator,
		typename OutputRAIterator,
		typename Comparator = std::less<typename InputRAIterator::value_type>>
	void mergeSort(
		InputRAIterator first,
		InputRAIterator last,
		OutputRAIterator out,
		Execution policy = Execution::PAR,
		Comparator cmp = Comparator()) {
		switch(policy) {
			case Execution::SEQ: _mergeSortSeq(first, last, out, cmp);
				break;
			default: _mergeSortPar(first, last, out, cmp);
				break;
		}
	}

	/**
	 * Merge sort
	 * @tparam InputContainer input container type
	 * @tparam OutputContainer output container type
	 * @tparam Comparator compare operator type
	 * @param in container to sort
	 * @param out container to store sorted input in
	 * @param policy execution policy
	 * @param cmp compare operator used for sorting
	 */
	template <
		typename InputContainer,
		typename OutputContainer,
		typename Comparator = std::less<typename InputContainer::value_type>>
	void mergeSort(
		const InputContainer& in,
		OutputContainer& out,
		Execution policy = Execution::PAR,
		Comparator cmp = Comparator()) {
		mergeSort(in.begin(), in.end(), out.begin(), policy, cmp);
	}

	/**
	 * Merge sort
	 * @tparam Container container type
	 * @tparam Comparator compare operator type
	 * @param container container to sort
	 * @param policy execution policy
	 * @param cmp compare operator used for sorting
	 */
	template <typename Container, typename Comparator = std::less<typename Container::value_type>>
	void mergeSort(Container& arr, Execution policy = Execution::PAR, Comparator cmp = Comparator()) {
		mergeSort(arr, arr, policy, cmp);
	}
}

#endif /* BOROVMI5_MERGE_SORT_H */